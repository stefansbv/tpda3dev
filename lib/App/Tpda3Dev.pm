package App::Tpda3Dev;

# ABSTRACT: Create and update Tpda3 application distributions

use 5.0100;
use utf8;
use Moose;
use Path::Tiny;
use Path::Iterator::Rule;
use MooseX::App 1.34 qw(Color Version);      # permute
use App::Tpda3Dev::Config;

app_namespace 'App::Tpda3Dev::Command';

app_permute 1;                     # allow use: --table table1 table2

option 'dryrun' => (
    is            => 'rw',
    isa           => 'Bool',
    documentation => q[Simulate command execution.],
);

option 'verbose' => (
    is            => 'rw',
    isa           => 'Bool',
    documentation => q[Verbose output.],
);

has config => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Config',
    lazy    => 1,
    default => sub {
        App::Tpda3Dev::Config->new;
    }
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 Interface

=head2 Attributes

=head3 dryrun

An attribute that holds the C<dryrun> comman line option.

=head3 verbose

An attribute that holds the C<verbose> comman line option.

=head3 config

Creates and returns an instance object of the
L<App::Tpda3Dev::Config> class.

=cut

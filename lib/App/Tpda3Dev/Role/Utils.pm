package App::Tpda3Dev::Role::Utils;

# ABSTRACT: Role for utility functions

use 5.0100;
use utf8;
use Moose::Role;
use Parse::RecDescent;

sub sort_hash_by_pos {
    my ( $self, $attribs ) = @_;

    #-- Sort by pos
    #- Keep only key and pos for sorting
    my %temp = map { $_ => $attribs->{$_}{pos} } keys %{$attribs};

    #- Sort with  ST
    my @attribs = map { $_->[0] }
        sort { $a->[1] <=> $b->[1] }
        map { [ $_ => $temp{$_} ] }
        keys %temp;

    return wantarray ? @attribs : \@attribs;
}

sub trim {
    my ( $self, @text ) = @_;
    for (@text) {
        s/^\s+//;
        s/\s+$//;
    }
    return wantarray ? @text : "@text";
}

sub parse_typenames {
    my ($self, $data_type) = @_;

    my $grammar = <<'__EOI__';

startrule : type_precision_scale
          | type_length
          | type_only

type_precision_scale : type_name '(' precision ',' scale ')'
    {
        $return = {
            type_name => $item{type_name},
            precision => $item{precision},
            scale     => $item{scale},
        }
    }

type_length : type_name '(' len ')'
    {
        $return = {
            type_name => $item{type_name},
            length    => $item{len},
        }
    }

type_only : type_name
    {
        $return = { type_name => $item{type_name} }
    }

precision : integer

scale : integer

len : integer

type_name : /\w+(\s\w+)?/i

integer : /\d+/

__EOI__

    my $parser = Parse::RecDescent->new($grammar);
    my $rec    =  $parser->startrule($data_type);
    return $rec;
}

no Moose::Role;

1;

__END__

=encoding utf8

=head1 NAME

App::Tpda3Dev::Role::Utils - A role for some utility functions

=head1 SYNOPSIS

  package App::Tpda3Dev::...
  with 'App::Tpda3Dev::Role::Utils';

=head1 DESCRIPTION

This role encapsulates common functions.

=head1 Interface

=head2 CLASS METHODS

=head2 sort_hash_by_pos

Use ST to sort hash by value (pos), returns an array or an array
reference of the sorted items.

=head2 trim

Trim strings or arrays.

=head3 parse_typenames

Parse the type names using a grammar and return the result as a hash
reference, where the keys are: type_name, precision, scale and length.

;) Maybe a little to overdone...

=head1 AUTHOR

Ștefan Suciu <stefan@s2i2.ro>

=cut

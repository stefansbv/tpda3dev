package App::Tpda3Dev::Utils;

# ABSTRACT: Utility methods

use 5.010001;
use utf8;
use Moose;
use Try::Tiny;
use Path::Iterator::Rule;
use Path::Tiny;
use Moose::Util::TypeConstraints;
use Locale::TextDomain qw(App-Tpda3Dev);
use App::Tpda3Dev::X qw(hurl);
use namespace::autoclean;

sub file_list {
    my ($self, $src_path) = @_;
    $src_path = path $src_path;
    my $rule = Path::Iterator::Rule->new;
    $rule->skip_vcs;
    $rule->skip(
        $rule->new->file->empty,
    );
    $rule->min_depth(1);
    my $next = $rule->iter( $src_path,
        { relative => 1, sorted => 1, follow_symlinks => 0 } );
    my $dirs = [];
    while ( defined( my $item = $next->() ) ) {
        my $dir = path($src_path, $item)->absolute;
        push @{$dirs}, $dir;
    }
    return $dirs;
}

sub find_files_perl {
    my ($self, $src_path) = @_;
    $src_path = path $src_path;
    my $rule = Path::Iterator::Rule->new;
    $rule->skip_vcs;
    $rule->skip(
        $rule->new->file->empty,
    );
    $rule->min_depth(1)->max_depth(1);
    $rule->perl_module;
    my $next = $rule->iter( $src_path,
        { relative => 1, sorted => 1, follow_symlinks => 0 } );
    my $dirs = [];
    while ( defined( my $item = $next->() ) ) {
        my $dir = path($src_path, $item)->absolute;
        push @{$dirs}, $dir;
    }
    return $dirs;
}

sub is_selfsame {
    my ( $self, $src_path, $dst_path ) = @_;
    if ( !$dst_path->is_file ) {
        return 0;
    }

    # Compare names
    return 0 if $src_path->basename ne $dst_path->basename;

    # Compare sizes
    return 0 if $self->file_stat($src_path)->size != $self->file_stat($dst_path)->size;

    # Check contents
    my $digest_src = $self->digest_local($src_path);
    my $digest_dst = $self->digest_local($dst_path);
    return ( $digest_src eq $digest_dst ) ? 1 : 0;
}

sub file_stat {
    my ( $self, $file ) = @_;
    return $file->stat;
}

sub file_perms {
    my ( $self, $file ) = @_;
    my $stat = $self->file_stat($file);
    return $stat->mode;
}

sub digest_local {
    my ($self, $file) = @_;
    my $digest;
    try   { $digest = $file->digest('MD5') }
    catch {
        my $err = $_;
        if ( $err =~ m{permission}i ) {
            hurl info => __x( "[EE] Permision denied for path: {file}.", file => $file );
        }
        else { die "Unknown error: $err" }
    };
    return $digest;
}

sub make_path {
    my ($self, $dir) = @_;
    try   { $dir->mkpath }
    catch {
        my $err = $_;
        my $logmsg = '';
        if ( $err =~ m{Permission denied}i ) {
            $logmsg = 'Permission denied';
        }
        else {
            $logmsg = $err;
        }
        hurl info => __x ( "[EE] The mkpath command failed: {msg}.", msg => $logmsg );
    };
    return;
}

sub copy_file_local {
    my ( $self, $src, $dst ) = @_;
    try { $src->copy($dst) }
    catch {
        my $err    = $_;
        my $logmsg = '';
        if ( $err =~ m{Permission denied}i ) {
            $logmsg = 'Permission denied';
        }
        else {
            $logmsg = $err;
        }
        hurl info => __x(
            "[EE] The copy command failed for {file}: {msg}.",
            file => $src,
            msg  => $logmsg,
        );
    };
    return;
}

sub set_perm {
    my ($self, $file, $perm) = @_;
    die "The 'set_perm' method works only with files." unless $file->is_file;
    try   { $file->chmod($perm) }
    catch {
        my $err = $_;
        my $logmsg = '';
        if ( $err =~ m{Operation not permitted}i ) {
            $logmsg = 'Permission denied';
        }
        else {
            $logmsg = $err;
        }
        hurl info => __x(
            "[EE] The perm command failed for {file}: {msg}.",
            file => $file,
            msg  => $logmsg,
        );
    };
    return;
}

sub set_owner {
    my ( $self, $file, $user ) = @_;
    die "The 'change_owner' method works only with files."
        unless $file->is_file;
    my ( $login, $pass, $uid, $gid ) = getpwnam($user)
        or die "$user not in passwd file";
    try   { chown $uid, $gid, $file->stringify }
    catch {
        hurl info => __x(
            "[EE] The chown command failed for {file}: {msg}.",
            file => $file,
            msg  => $_,
        );
    };
    return;
}

sub get_perms {
    my ( $self, $file ) = @_;
    my $mode = try { $self->file_perms($file) }
    catch  {
        my $err = $_;
        if ( $err =~ m/Permission denied/i ) {
            hurl info => __x ( "[EE] Permision denied for path: {file}.", file => $file );
        }
        elsif ( $err =~ m/No such file or directory/i ) {
            hurl info => __x ( "[EE] No such file or directory: {file}.", file => $file );
        }
        else {
            die "Unknown stat ERROR: $err";
        }
    };
    return sprintf "%04o", $mode & 07777;
}

sub get_owner {
    my ( $self, $file ) = @_;
    my $uid = try { $file->stat->uid }
        catch  {
        my $err = $_;
        if ( $err =~ m/Permission denied/i ) {
            hurl info => __x ( "[EE] Permision denied for path: {file}.", file => $file );
        }
        elsif ( $err =~ m/No such file or directory/i ) {
            hurl info => __x ( "[EE] No such file or directory: {file}.", file => $file );
        }
        else {
            die "Unknown stat ERROR: $err";
        }
    };
    # my $user = ( getpwuid $uid )[0];
    return ( getpwuid $uid )[0];
    # return $user;
}

__PACKAGE__->meta->make_immutable;

1;
__END__

=encoding utf8

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 INTERFACE

=head2 ATTRIBUTES

=head2 INSTANCE METHODS

=head3 copy_file

=head3 file_list

Return the list of the absolute paths of the files from a directory.

=head3 find_files_perl

Return the list of Perl modules in a directory.

=head3 is_selfsame

Try to determine if two files with the same name but in different
paths are identical.  First compare the file names, return false if
the names differ, then the file size, return false if the sizes differ
and finally compare the MD5 digest of the contents and return the
result.

=head3 file_stat

Return the stat of a file.

=head3 file_perms

Return the perms of a file in octal.

=head3 digest_local

Compute and return a MD5 digest.

=head3 make_path

Create a path.

=head3 copy_file_local

Copy files from and to the local filesystem.

=head3 set_perm

Change the perms of a file.

=head3 set_owner

Change the owner of a file.

=head3 get_perms

Return the perms of a file.

=head3 get_owner

=cut

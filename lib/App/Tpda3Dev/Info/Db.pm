package App::Tpda3Dev::Info::Db;

# ABSTRACT: Collect and transform database info

use 5.010001;
use utf8;
use Moose;
use Path::Tiny;
use Try::Tiny;
use Locale::TextDomain qw(App-Tpda3Dev);
use App::Tpda3Dev::X qw(hurl);
use App::Tpda3Dev::Info::Dist;
use App::Tpda3Dev::Info::Db::Connection;
use App::Tpda3Dev::Target;
use namespace::autoclean;

use Data::Dump qw/dump/;

has 'main_table' => (
    is  => 'ro',
    isa => 'Maybe[Str]',
);

has 'dep_tables' => (
    is      => 'ro',
    isa     => 'Maybe[ArrayRef]',
    default => sub { [] },
);

has 'conn_yaml_file' => (
    is  => 'ro',
    isa => 'Maybe[Path::Tiny]',
);

has 'uri' => (
    is  => 'rw',
    isa => 'Str',
);

has 'debug' => (
    is  => 'ro',
    isa => 'Bool',
);

#--

has 'info_conn' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info::Db::Connection',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return App::Tpda3Dev::Info::Db::Connection->new(
            conn_yaml_file => $self->conn_yaml_file,
        );
    },
);

has 'target' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Target',
    lazy    => 1,
    default => sub {
        my $self = shift;
        $self->info_conn->dbname; # need to call this before 'uri'
        return App::Tpda3Dev::Target->new(
            uri => $self->info_conn->uri,
        );
    },
);

has 'tpda3_column_types_map' => (
    traits  => ['Hash'],
    is      => 'ro',
    isa     => 'HashRef[Str]',
    default => sub {
        return {
            'blob'              => 'alphanumplus',
            'char'              => 'alpha',
            'character varying' => 'alphanumplus',
            'd_float'           => 'numeric',
            'date'              => 'date',
            'decimal'           => 'numeric',
            'double'            => 'numeric',
            'double precision'  => 'numeric',
            'float'             => 'numeric',
            'int64'             => 'integer',
            'integer'           => 'integer',
            'numeric'           => 'numeric',
            'smallint'          => 'integer',
            'text'              => 'alphanumplus',
            'time'              => 'time',
            'timestamp'         => 'timestamp',
            'timestamptz'       => 'timestamp',
            'varchar'           => 'alphanumplus',
        };
    },
    handles => {
        get_type => 'get',
    },
);

has maintable_info => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_build_maintable_info',
);

sub _build_maintable_info {
    my $self = shift;
    my $engine     = $self->target->engine;
    my $main_table = $self->main_table;
    hurl maintable_info => __("The main table name should be provided!")
        unless $main_table;
    hurl maintable_info =>
        __x( "The main table '{table}' does not exists!",
             table => $main_table )
        unless $engine->table_exists($main_table);
    my $main_info  = $engine->get_info( $main_table, 'pos' );
    my ( @columns, %info );
    foreach my $position ( sort { $a <=> $b } keys %{$main_info} ) {
        my $name = $main_info->{$position}{name};
        $info{$name} = $main_info->{$position};
        push @columns, $name;
    }
    my $pk_keys = $engine->table_keys($main_table);
    my $fk_keys = $engine->table_keys( $main_table, 'foreign' );
    return {
        name    => $main_table,
        pk_keys => $pk_keys,
        fk_keys => $fk_keys,
        collist => \@columns,
        colinfo => \%info,
    };
}

has deptables_info => (
    is      => 'ro',
    isa     => 'ArrayRef[HashRef]',
    lazy    => 1,
    builder => '_build_deptables_info',
);

sub _build_deptables_info {
    my $self = shift;
    my $engine = $self->target->engine;
    my $tables = $self->dep_tables;
    if ($self->debug) {
        print "_build_deptables_info:\n";
        print "tables:\n";
        dump $tables;
        print "---\n";
    }
    return [] unless defined $tables;
    my @deptables;
    foreach my $table ( @{$tables} ) {
        my $dep_info = $engine->get_info( $table, 'pos' );
        if ($self->debug) {
            print "dep_info:\n";
            dump $dep_info;
            print "---\n";
        }
        my ( @columns, %info );
        foreach my $position ( sort { $a <=> $b } keys %{$dep_info} ) {
            my $name = $dep_info->{$position}{name};
            $info{$name} = $dep_info->{$position};
            push @columns, $name;
        }
        my $pk_keys = $engine->table_keys($table);
        my $fk_keys = $engine->table_keys( $table, 'foreign' );
        my $table_info = {
            name    => $table,
            pk_keys => $pk_keys,
            fk_keys => $fk_keys,
            collist => \@columns,
            colinfo => \%info,
        };
        push @deptables, $table_info
    }
    return  \@deptables;
}

sub maintable_data {
    my $self = shift;

    my $maintable_info = $self->maintable_info;
    my $table          = $maintable_info->{name};
    my $keyfields      = $maintable_info->{pk_keys};
    my $collist        = $maintable_info->{collist};
    my $pkcol          = $keyfields->[0];
    hurl info_conn =>
        __x( "The '{table}' main table doesn't have a primary key",
        table => $table )
        unless $pkcol;

    my $record = {};
    $record->{name}      = $table;
    $record->{view}      = $table;       # "v_$table" -> VIEW name
    $record->{keyfields} = $keyfields;
    $record->{collist}   = $collist;

    # Column data for the 'maintable' screen config section
    foreach my $column ( @{$collist} ) {
        my $info  = $maintable_info->{colinfo}{$column};
        my $type  = $info->{type};
        my $state = $pkcol eq $column ? 'disabled' : 'normal';

        my $col = {};
        $col->{name}        = $column;
        $col->{label}       = $self->label($column);
        $col->{state}       = $state;
        $col->{ctrltype}    = $self->ctrltype($info);
        $col->{displ_width} = $self->len($info);
        $col->{valid_width} = $self->len($info);
        $col->{numscale}    = $self->numscale( $info->{scale} );
        $col->{readwrite}   = 'rw';
        $col->{findtype}    = 'full';
        $col->{bgcolor}     = 'white';
        $col->{datatype}    = $self->datatype($type);

        push @{ $record->{columns} }, $col;
    }

    return $record;
}

sub deptables_data {
    my $self = shift;

    my @tables_data;
    foreach my $table_info ( @{ $self->deptables_info } ) {
        my $table     = $table_info->{name};
        my $keyfields = $table_info->{pk_keys};
        my $collist   = $table_info->{collist};

        my $record = {};
        $record->{name}      = $table;
        $record->{view}      = $table;       # "v_$table" -> VIEW name
        $record->{keyfields} = $keyfields;
        $record->{collist}   = $collist;
        $record->{orderby}   = $keyfields;

        # Column data for the 'deptable' screen config section
        foreach my $column ( @{$collist} ) {
            my $info = $table_info->{colinfo}{$column};
            my $type = $info->{type};
            my $id   = $info->{pos} + 1;

            my $col = {};
            $col->{name}        = $column;
            $col->{id}          = $id;
            $col->{label}       = $self->label($column);
            $col->{tag}         = 'ro_center';
            $col->{displ_width} = $self->len($info);
            $col->{valid_width} = $self->len($info);
            $col->{numscale}    = $self->numscale( $info->{scale} );
            $col->{readwrite}   = 'rw';
            $col->{datatype}    = $self->datatype($type);

            push @{ $record->{columns} }, $col;
        }
        push @tables_data, $record;
    }

    return \@tables_data;;
}

sub ctrltype {
    my ($self, $info) = @_;

    my $type = lc $info->{type};
    my $len  = $info->{length} // 10;
    #                when column type is ...            ctrl type is ...
    return  $type eq q{}                              ? 'x'
         :  $type eq 'blob'                           ? 't'
         :  $type eq 'date'                           ? 'd'
         :  $type eq 'character'                      ? 'e'
         :  $type eq 'text'                           ? 't'
         : ($type eq 'varchar' and $len > 200 )       ? 't'
         :                                              'e'
         ;
}

sub numscale {
    my ($self, $scale) = @_;
    return defined $scale ? $scale : 0;
}

sub len {
    my ($self, $info) = @_;

    my $type = lc $info->{type};
    my $len  = $info->{length} // $type eq 'text' ? 30 : 10;

    my $max_len = 30;           # max length, hardwired config
    my $min_len = 5;            # min length, hardwired config

    return
       $len >= $max_len  ? $max_len
     : $len <= $min_len  ? $min_len
     :                     $len
     ;
}

sub datatype {
    my ( $self, $db_type ) = @_;
    $db_type = lc $db_type;
    my $tpda_type = $self->get_type($db_type);
    return $tpda_type ? $tpda_type : 'alphanumplus';
}

sub label {
    my ($self, $label) = @_;
    $label = ucfirst $label;
    $label =~ s{_}{ }g;
    return $label;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 SYNOPSIS


=head1 DESCRIPTION

Collect and transform database info for the render modules.

=head1 INTERFACE

=head2 ATTRIBUTES

=head3 main_table

=head3 dep_tables

=head3 conn_yaml_file

=head3 uri

=head3 info_conn

=head3 target

=head3 tpda3_column_types_map

=head3 maintable_info

Collect database info and return it as a complex record.

=head3 deptables_info

=head2 INSTANCE METHODS

=head3 maintable_data

=head3 deptables_data

=head3 ctrltype

Control type.  The numeric and integer types => Tk::Entry.  The char
type is good candidate for Tk::JComboBox entries (m).  And of course
the date type for Tk::DateEntry.

If the length of the column is greater than B<200> make it a text
entry.

=head3 numscale

Numeric scale.

=head3 len

Length of the field in chars.

=head3 datatype

Column type name

=head3 remove_dupes

=head3 label

=cut

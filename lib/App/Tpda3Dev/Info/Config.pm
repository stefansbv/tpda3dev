package App::Tpda3Dev::Info::Config;

# ABSTRACT: Tpda3 application config related info

use 5.010001;
use Moose;

use Tpda3::Config;

has 'mnemonic' => (
    is  => 'ro',
    isa => 'Str',
);

has 'tpda' => (
    is      => 'ro',
    isa     => 'Tpda3::Config',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $args = {
            cfname => $self->mnemonic,
            user   => undef,
            pass   => undef,
        };
        return Tpda3::Config->instance($args);
    },
);

has 'apps_dir' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->tpda->cfapps;
    },
);

has 'module' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->tpda->application->{module};
    },
);

sub BUILD {
    my $self = shift;
    my $toolkit = $self->tpda->application->{widgetset};
    die "Fatal!: $toolkit toolkit not supported!"
        unless $toolkit eq 'Tk';
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=pod

=head2 new

Constructor.

=head2 config_info

Application configuration info.

=cut

package App::Tpda3Dev::Info::Dist;

# ABSTRACT: A Tpda3 application distribution info

use 5.010001;
use utf8;
use Moose;
use File::UserConfig;
use Path::Tiny qw(cwd path);
use File::ShareDir qw(dist_dir);
use Try::Tiny;
use Tpda3::Config::Utils;
use App::Tpda3Dev::X qw(hurl);
use App::Tpda3Dev::Utils;
use namespace::autoclean;

has 'check_mark' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {
        return $^O eq 'MSWin32' ? " yes" : " ✔";
    },
);

has 'module' => (
    is  => 'rw',
    isa => 'Maybe[Str]',
);

has 'dist_path' => (
    is       => 'rw',
    isa      => 'Path::Tiny',
    required => 0,
    default  => sub {
        return path('.');
   },
);

has 'mnemonic' => (
    is      => 'rw',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return lc $self->module if $self->module;
        return lc $self->find_app_name;
    },
);

has 'dist_name' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self   = shift;
        my $module = $self->module;
        die "No module name, can't provide 'dist_name'!" unless $module;
        return qq{Tpda3-$module};
    },
);

has 'module_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    default => sub {
        return path( qw(lib Tpda3 Tk App) );
    },
);

has 'screen_module_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return path( $self->module_path, $self->module );
    },
);

has 'util' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Utils',
    lazy    => 1,
    default => sub {
        return App::Tpda3Dev::Utils->new;
    },
);

sub screen_module_list {
    my $self = shift;
    my $screen_path = $self->screen_module_path;
    my $files = $self->util->find_files_perl($screen_path);
    if ( scalar @{$files} == 0 ) {
        return [];
    }
    else {
        my @modules;
        foreach my $file ( @{$files} ) {
            if ( $file->is_file ) {
                # TODO: check if is a screen module! XXX
                my $rel = path($file)->relative($screen_path);
                push @modules, $rel;
            }
        }
        return \@modules;
    }
}

sub list_screen_modules {
    my $self = shift;
    my $scrmodlst = $self->screen_module_list;
    foreach my $screen ( @{$scrmodlst} ) {
        print "   > $screen\n";
    }
    return;
}

has 'testdir' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    default => sub {
        return path( 't/' );
    },
);

has 'sys_sharedir' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    default => sub {
        return path( qw(share) );
    },
);

has 'dist_sharedir' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    default => sub {
        return path( qw(share apps) );
    },
);

has 'dirtree' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    lazy    =>1,
    default => sub {
        my $self = shift;
        my $dirtree;
        try {
            $dirtree = path( dist_dir('App-Tpda3Dev'), 'dirtree' );
        }
        catch {
            $dirtree = path( 'share', 'dirtree');
        };
        return $dirtree;
    },
);

has 'templates' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    default => sub {
        my $sharedir;
        try {
            $sharedir = path( dist_dir('App-Tpda3Dev'), 'templates' );
        }
        catch {
            $sharedir = path( 'share', 'templates');
        };
    },
);

has 'configdir' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    default => sub {
        my $configdir = File::UserConfig->new(
            dist     => 'Tpda3',
            sharedir => 'share',
        )->configdir;
        return path($configdir);
    },
);

has 'context' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return 'update' if $self->is_app_dir;
        return 'new';
    },
);

sub config_src {
    my $self = shift;
    return path( $self->dirtree, 'config' );
}

sub module_src {
    my $self = shift;
    return path( $self->dirtree, 'module' );
}

sub module_path_exists {
    my $self = shift;
    my $path = $self->module_path;
    return 1 if $path->is_dir;
    return;
}

sub relative_path_to_dist {
    my ($self, @path) = @_;
    die "No parameter provided for 'relative_path_to_dist'" unless scalar @path;
    return path( $self->dist_path, $self->dist_name, @path );
}

sub find_app_name {
    my $self = shift;
    my $app_path = $self->module_path if $self->module_path_exists;
    hurl "Can't determine the distribution name, wrong context?"
        unless ($app_path);
    my $filelist = Tpda3::Config::Utils->find_files( $app_path, 'pm' );
    my $no = scalar @{$filelist};
    if ( $no == 0 ) {
        die "Can't get the application name in: '$app_path'\n";
        return;
    }
    ( my $appname = $filelist->[0] ) =~ s{\.pm$}{};    # XXX should be only one
    $self->module($appname);
    return $appname;
}

sub find_mnemonic {
    my $self = shift;
    my $cfg_path = $self->dist_sharedir if $self->dist_sharedir->is_dir;
    return unless $cfg_path;
    my $dirlist = Tpda3::Config::Utils->find_subdirs($cfg_path);
    my $no = scalar @{$dirlist};
    if ( $no == 0 ) {
        die "Can't get the application share dir in: '$cfg_path'\n";
        return;
    }
    my $mnemonic = $dirlist->[0];            # XXX should be only one
    $self->mnemonic($mnemonic);
    return $mnemonic;
}

sub testdir_exists {
    my ($self, $dir) = @_;
    return 1 if $self->testdir->is_dir;
    return;
}

sub user_path_for {
    my ($self, $dir, $mnemo) = @_;
    my $mnemonic = $mnemo || $self->mnemonic;
    die "No parameter provided for 'user_path_for'" unless $dir;
    return path( $self->configdir, 'apps', $mnemonic, $dir )
        if $self->configdir && $mnemonic;
    return;
}

sub user_sys_path_for {
    my ( $self, $dir ) = @_;
    die "No parameter provided for 'user_sys_path_for'" unless $dir;
    return path( $self->configdir, $dir )
      if $self->configdir;
    return;
}

sub check {
    my ($self, $bool) = @_;
    return $bool ? $self->check_mark : " no";
}

sub exists_user_path_for {
    my ($self, $dir) = @_;
    return 1 if $self->user_path_for($dir)->is_dir;
    return;
}

sub exists_user_sys_path_for {
    my ($self, $dir) = @_;
    return 1 if $self->user_sys_path_for($dir)->is_dir;
    return;
}

sub dist_path_for {
    my ($self, $dir, $mnemo) = @_;
    my $mnemonic = $mnemo || $self->mnemonic;
    die "No parameter provided for 'dist_path_for'" unless $dir;
    if ( $self->configdir && $mnemonic ) {
        return path $self->dist_sharedir, $mnemonic, $dir;
    }
    return;
}

sub dist_sys_path_for {
    my ($self, $dir) = @_;
    die "No parameter provided for 'dist_sys_path_for'" unless $dir;
    if ( $self->configdir ) {
        return path $self->sys_sharedir, $dir;
    }
    return;
}

sub exists_dist_path_for {
    my ($self, $dir) = @_;
    return 1 if $self->dist_path_for($dir)->is_dir;
    return;
}

sub exists_dist_sys_path_for {
    my ($self, $dir) = @_;
    return 1 if $self->dist_sys_path_for($dir)->is_dir;
    return;
}

sub is_app_dir {
    my $self = shift;
    return (   $self->module_path_exists
            && $self->exists_dist_path_for('etc') );
}

sub list_config_files {
    my $self = shift;
    my $scrdir = path $self->dist_path_for('scr');
    my $scrlst = Tpda3::Config::Utils->find_files($scrdir, 'conf');
    print "Screen configurations:\n";
    foreach my $cfg_name ( @{$scrlst} ) {
        print " > $cfg_name\n";
    }
    print " in $scrdir\n";
    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 SYNOPSIS

=head1 DESCRIPTION

Info about an Tpda3 distribution tree.

There are two major cases, determined by the CWD:

1. The CWD is a dir containg a Tpda3 distribution.

This is the right context for creating new screens and configurations,
but the wrong context for creating new distributions.

2. The CWD is a dir that doesn't contain a Tpda3 distribution.

This is the right context for creating new distributions.

=head1 INTERFACE

=head2 ATTRIBUTES

=head3 check_mark

=head3 module

Set and get the module.  The name of the main module of the
application located in the C<module_path> dir.

=head3 dist_path

=head3 mnemonic

Return the name of the directory under L<dist_sharedir>.  By default
it is a lower case of the C<module> attribute.

=head3 dist_name

Return the name obtained by concatenating the C<Tpda3> string with the
name of the main module.

=head3 module_path

Returns a relative path to the main module of a Tpda3 application.
distribution.

    L<Tpda3-Name/lib/Tpda3/Tk/App>

=head3 screen_module_path

Return the relative path to the screen modules of a Tpda3 application.

=head3 testdir

Return the path for the tests.

=head3 sys_sharedir

=head3 dist_sharedir

Returns the relative path to the configurations of Tpda3 applications.

    L<Tpda3-Name/share/apps>

=head3 dirtree

=head3 templates

=head3 configdir

Returns the path to the user configurations.

For example, on my box:

    L</home/user/.local/share/.tpda3/apps>

=head3 context

=head3 util

The util attribute holds an instance object of the
C<App::Tpda3Dev::Utils> class.

=head2 INSTANCE METHODS

=head3 screen_module_list

=head3 list_screen_modules

=head3 config_src

=head3 module_src

=head3 module_path_exists

Return true if C<module_path> exists.

=head3 relative_path_to_dist

Returns a path relative to the new dist.

=head2 find_app_name

First method to call for existing distributions in the right context.

Find and return the current application name - the name of the main
module.

First check if a subdirectory exists in C<$app_path>, then check if a
module exists, if true, return the name.

=head3 find_mnemonic

If a directory under <dist_sharedir> with the name being lower case of
the module name does not exists, find the name of the existing dir and
update the C<mnemonic> attribute.

=head3 testdir_exists

Returns true if the test dir exists.

=head3 user_path_for

Return the user configurations path.

=head3 user_sys_path_for

=head3 check

Display a check mark if the dist share path exists.

=head3 exists_user_path_for

Returns true if the user configurations path exists.

=head3 exists_user_sys_path_for

=head3 dist_path_for

=head3 dist_sys_path_for

=head3 exists_dist_path_for

=head3 exists_dist_sys_path_for

=head2 is_app_dir

Return true if CWD is a Tpda3 application distribution dir.

=head3 list_config_files

List the screen configuration files.

=cut

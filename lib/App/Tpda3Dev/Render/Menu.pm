package App::Tpda3Dev::Render::Menu;

# ABSTRACT: Create a menu file

use 5.010001;
use utf8;
use Moose;

extends qw(App::Tpda3Dev::Render);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'menu'},
);

has menudata => (
    is       => 'ro',
    isa      => 'ArrayRef',
    required => 1,
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
    default  => sub {
        return 'menu.yml';
    },
);

has 'info' => (
    is       => 'ro',
    isa      => 'App::Tpda3Dev::Info',
    required => 0,
);

has 'output_path' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    lazy     => 1,
    default  => sub {
        my $self    = shift;
        return $self->info->dist->dist_path_for('etc');
    },
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return {
            menudata => $self->menudata,
        };
    },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=pod

=head2 new

Constructor.

=head2 generate_test

Generate test from template.

=cut

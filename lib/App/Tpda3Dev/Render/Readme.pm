package App::Tpda3Dev::Render::Readme;

# ABSTRACT: Create the main module of the app

use 5.010001;
use utf8;
use Moose;

extends qw(App::Tpda3Dev::Render);

has 'module' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'readme'},
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 0,
    default  => sub {
        my $self = shift;
        my ($user_name, $user_email) = $self->config->get_gitconfig;
        return {
            module      => $self->module,
            copy_author => $user_name,
            copy_email  => $user_email,
            copy_year   => (localtime)[5] + 1900,
        };
    },
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Str',
    default  => sub {
        return 'README';
    },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=pod

=head2 new

Constructor.

=head2 generate_module

Generate the main application module from template.

=cut

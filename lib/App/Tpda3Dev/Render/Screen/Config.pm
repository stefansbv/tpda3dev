package App::Tpda3Dev::Render::Screen::Config;

# ABSTRACT: Create a screen configuration file

use 5.010001;
use utf8;
use Moose;
use Config::General;
use Tie::IxHash::Easy;
use List::Compare;
use Path::Tiny;

use App::Tpda3Dev::Info::Dist;

extends qw(App::Tpda3Dev::Render);

has 'screen' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'dist_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
);

has 'maintable_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

has 'deptables_data' => (
    is       => 'ro',
    isa      => 'Maybe[ArrayRef[HashRef]]',
    required => 0,
);

has 'info' => (
    is       => 'ro',
    isa      => 'App::Tpda3Dev::Info',
    required => 0,
);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'config'},
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Maybe[Str]',
    required => 1,
    lazy     => 1,
    default  => sub {
        my $self    = shift;
        my $screen  = lc $self->screen;
        my $scrconf = ( $screen =~ m{\.conf$} ) ? $screen : "$screen.conf";
        return $scrconf;
    },
);

has 'output_path' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    required => 1,
    lazy     => 1,
    default  => sub {
        my $self    = shift;
        return $self->info->dist->dist_path_for('scr');
    },
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_build_templ_data',
);

sub _build_templ_data {
    my $self           = shift;
    my $maintable_data = $self->maintable_data;
    my $keyfields      = $maintable_data->{keyfields};
    my $collist        = $maintable_data->{collist};
    my $columns        = $self->remove_keyfields( $keyfields, $collist );
    return {
        screen    => $self->screen,
        maintable => $self->maintable_data,
        deptables => $self->deptables_data,
        keyfields => $keyfields,
        columns   => $columns,
    };
}

sub _config_string {
    my ($self, $rec) = @_;
    my $conf = Config::General->new(
        -AllowMultiOptions => 1,
        -SplitPolicy       => 'equalsign',
        -Tie               => "Tie::IxHash",
    );
    return $conf->save_string($rec);
    return;
}

sub remove_keyfields {
    my ($self, $pkfields, $fields) = @_;
    my $lc = List::Compare->new( $pkfields, $fields );
    my @columns = $lc->get_complement;
    return \@columns;
}

1;

__END__

=pod

=head2 new

Constructor.

=head2 _init

Initializations.

=head2 generate_config

Prepare data for the screen configuration file and create the new
file.

=head2 maintable_data

Generate the L<maintable> section of the config file.

=head2 prepare_config_data_dep

Generate the L<deptable> section of the config file.

=head2 render_config

Generate a module configuration file.

Parameters:

The screen configuration file name and the configuration data.

=head1 DEFAULTS

Subs to handle defaults

=head2 ctrltype

Control type.  The numeric and integer types => Tk::Entry.  The char
type is good candidate for Tk::JComboBox entries (m).  And of course
the date type for Tk::DateEntry.

If the length of the column is greater than B<200> make it a text
entry.

=head2 numscale

Numeric scale.

=head2 len

Length of the field in chars.

=head2 datatype

Column type.

=head2 remove_keyfields

Remove the key fields from the list to avoid duplicates.

=head2 label

Remove underscores form label and make the first character upper case.

=cut

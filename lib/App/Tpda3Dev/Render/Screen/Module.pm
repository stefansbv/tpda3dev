package App::Tpda3Dev::Render::Screen::Module;

# ABSTRACT: Create a screen module file

use 5.010001;
use utf8;
use Moose;
use Config::General qw{ParseConfig};
use Path::Tiny;
use Tie::IxHash;

extends qw(App::Tpda3Dev::Render);

has 'screen' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'module' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'screen'},
);

has 'info' => (
    is       => 'ro',
    isa      => 'App::Tpda3Dev::Info',
    required => 0,
);

has 'screen_config_file' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $name = lc $self->screen;
        return "$name.conf";
    },
);

has 'screen_config_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return path $self->info->dist->dist_path_for('scr'),
            $self->screen_config_file;
    },
);

has 'screen_config' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    default => sub {
        my $self = shift;
        my $config_file = $self->screen_config_path;
        die "Can't locate config file:\n$config_file" unless -f $config_file;
        tie my %cfg, "Tie::IxHash";          # keep the sections order
        %cfg = ParseConfig(
            -ConfigFile => $config_file,
            -Tie        => 'Tie::IxHash',
        );
        return \%cfg;
    },
);

has 'dist_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
);

has 'output_path' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    lazy     => 1,
    default  => sub {
        my $self    = shift;
        return $self->info->dist->screen_module_path;
    },
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        my ($user_name, $user_email) = $self->config->get_gitconfig;
        my $screen = $self->screen;
        return {
            module      => $self->module,
            screen      => $screen,
            copy_author => $user_name,
            copy_email  => $user_email,
            copy_year   => (localtime)[5] + 1900,
            conf        => lc("$screen.conf"),
            columns     => $self->screen_config->{maintable}{columns},
            pkcol       => $self->screen_config->{maintable}{pkcol}{name},
        };
    },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 Synopsis


=head1 Description


=head1 Interface

=head2 Attributes

=head3 screen

The name of the new screen.

=head3 module

=head3 templ_type

=head3 screen_config_file

=head3 screen_config_path

=head3 screen_config

=head3 dist_path

=head3 info

=head3 output_path

=head3 templ_data

=head2 Instance Methods

=cut

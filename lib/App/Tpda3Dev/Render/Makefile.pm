package App::Tpda3Dev::Render::Makefile;

# ABSTRACT: Create a Makefile.PL script

use 5.010001;
use utf8;
use Moose;

use App::Tpda3Dev::Config;
use App::Tpda3Dev::Info::Dist;

extends qw(App::Tpda3Dev::Render);

has 'module' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'makefile'},
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        my ($user_name, $user_email) = $self->config->get_gitconfig;
        return {
            module      => $self->module,
            copy_author => $user_name,
            copy_email  => $user_email,
            copy_year   => (localtime)[5] + 1900,
        };

    },
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
    default  => sub {
        return 'Makefile.PL';
    },
);

1;

__END__

=pod

=head2 new

Constructor.

=head2 generate_makefile

Generate screen module.

=cut

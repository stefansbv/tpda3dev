package App::Tpda3Dev::Render::PgTap;

# ABSTRACT: Create a SQL

use 5.010001;
use utf8;
use Moose;
use Path::Tiny;

use App::Tpda3Dev::Info::Dist;

extends qw(App::Tpda3Dev::Render);

has 'dist_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
);

has 'table' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'table_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'pgtap-table'},
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Maybe[Str]',
    required => 1,
    lazy     => 1,
    default  => sub {
        my $self    = shift;
        my $table = $self->table;
        return qq{pgtap-${table}.sql};
    },
);

has 'output_path' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    required => 1,
    lazy     => 1,
    default  => sub {
        return path '.';
    },
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_build_templ_data',
);

sub _build_templ_data {
    my $self = shift;
    my $meta = $self->table_data;
    my $data = {};
    foreach my $field ( keys %{$meta} ) {
        $data->{ $meta->{$field}{pos} } = $meta->{$field};
        my $type  = $meta->{$field}{type};
        next if $type eq 'integer';
        next if $type eq 'smallint';
        next if $type eq 'bigint';
        next if $type eq 'real';
        next if $type eq 'double precision';
        my $prec  = $meta->{$field}{prec};
        my $scale = $meta->{$field}{scale};
        if ( defined $prec && defined $scale ) {
            # print "fix $field: t=$type p=$prec s=$scale\n";
            $type = $type . '(' . $prec . ',' . $scale . ')';
        }
        elsif ( defined $prec && !defined $scale ) {
            # print "fix $field: t=$type p=$prec s=undef\n";
            $type = $type . '(' . $prec . ')';
        }
        elsif ( !defined $prec && defined $scale ) {
            # print "fix $field: t=$type p=undef s=$scale\n";
            $type = $type . '(' . $scale . ')';
        }
        elsif ( !defined $prec && !defined $scale ) {
            # say "not fixing $field: t=$type p=undef s=undef\n";
        }
        else {
            # This should not happen...
            die "$field: t=$type p=$prec s=$scale";
        }
        $meta->{$field}{type} = $type;
    }
    return {
        table => $self->table,
        meta  => $data,
    };
}

1;

__END__

=pod

=head2 new

Constructor.

=head2 _init

Initializations.

=head2 generate_config

Prepare data for the screen configuration file and create the new
file.

=head2 maintable_data

Generate the L<maintable> section of the config file.

=head2 prepare_config_data_dep

Generate the L<deptable> section of the config file.

=head2 render_config

Generate a module configuration file.

Parameters:

The screen configuration file name and the configuration data.

=head1 DEFAULTS

Subs to handle defaults

=head2 ctrltype

Control type.  The numeric and integer types => Tk::Entry.  The char
type is good candidate for Tk::JComboBox entries (m).  And of course
the date type for Tk::DateEntry.

If the length of the column is greater than B<200> make it a text
entry.

=head2 numscale

Numeric scale.

=head2 len

Length of the field in chars.

=head2 datatype

Column type.

=head2 remove_keyfields

Remove the key fields from the list to avoid duplicates.

=head2 label

Remove underscores form label and make the first character upper case.

=cut

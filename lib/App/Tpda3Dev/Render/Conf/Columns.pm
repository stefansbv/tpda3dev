package App::Tpda3Dev::Render::Conf::Columns;

# ABSTRACT: Create a columns.conf configuration file for the transfer app

use 5.010001;
use utf8;
use Moose;
use Config::General;
use Tie::IxHash::Easy;
use List::Compare;
use Path::Tiny;

use App::Tpda3Dev::Info::Dist;

extends qw(App::Tpda3Dev::Render);

has 'dist_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
);

has 'table_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

has 'info' => (
    is       => 'ro',
    isa      => 'App::Tpda3Dev::Info',
    required => 0,
);

has 'templ_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => sub {'columns-conf'},
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Maybe[Str]',
    required => 1,
    lazy     => 1,
    default  => sub {
        my $self    = shift;
        return 'columns.conf';
    },
);

has 'output_path' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    required => 1,
    lazy     => 1,
    default  => sub {
        return path '.';
    },
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    builder  => '_build_templ_data',
);

sub _build_templ_data {
    my $self = shift;
    my $meta = $self->table_data;
    my $data = {};
    foreach my $field ( keys %{$meta} ) {
        $data->{ $meta->{$field}{pos} } = $meta->{$field};
    }
    return { meta => $data };
}

sub _config_string {
    my ($self, $rec) = @_;
    my $conf = Config::General->new(
        -AllowMultiOptions => 1,
        -SplitPolicy       => 'equalsign',
        -Tie               => "Tie::IxHash",
    );
    return $conf->save_string($rec);
    return;
}

1;

__END__

=pod

=head2 new

Constructor.

=head2 _init

Initializations.

=head2 generate_config

Prepare data for the screen configuration file and create the new
file.

=head2 maintable_data

Generate the L<maintable> section of the config file.

=head2 prepare_config_data_dep

Generate the L<deptable> section of the config file.

=head2 render_config

Generate a module configuration file.

Parameters:

The screen configuration file name and the configuration data.

=head1 DEFAULTS

Subs to handle defaults

=head2 remove_keyfields

Remove the key fields from the list to avoid duplicates.

=head2 label

Remove underscores form label and make the first character upper case.

=cut

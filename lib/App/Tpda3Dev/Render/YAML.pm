package App::Tpda3Dev::Render::YAML;

# ABSTRACT: Create a YAML configuration file

use 5.010001;
use utf8;
use Moose;
use URI::db;
use Path::Tiny;

extends qw(App::Tpda3Dev::Render);

use App::Tpda3Dev::Info::Db::Connection;

has 'module' => (
    is  => 'ro',
    isa => 'Str',
);

has 'uri' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'info_db' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info::Db::Connection',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return App::Tpda3Dev::Info::Db::Connection->new(
            uri => $self->uri,
        );
    },
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return {
            module => $self->module,
            driver => $self->info_db->driver,
            host   => $self->info_db->host,
            dbname => $self->info_db->dbname,
            port   => $self->info_db->port,
            user   => $self->info_db->user,
            role   => $self->info_db->role,
        };
    },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=pod

=head2 new

Constructor.

=head2 generate_config

Generate a YAML config from a template.

=cut

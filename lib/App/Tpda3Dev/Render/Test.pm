package App::Tpda3Dev::Render::Test;

# ABSTRACT: Create a test file

use 5.010001;
use utf8;
use Moose;

extends qw(App::Tpda3Dev::Render);

has 'module' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'mnemonic' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return {
            module   => $self->module,
            mnemonic => $self->mnemonic,
        };
    },
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=pod

=head2 new

Constructor.

=head2 generate_test

Generate test from template.

=cut

package App::Tpda3Dev::Edit::Menu;

# ABSTRACT: Tpda3 menu config update

use 5.010001;
use utf8;
use Moose;
use Path::Tiny;
use Try::Tiny;
use Tpda3::Utils;
use namespace::autoclean;

has 'name' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'info' => (
    is       => 'ro',
    isa      => 'App::Tpda3Dev::Info',
    required => 0,
);

has 'menu_file_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $etc  = $self->info->dist->dist_path_for('etc');
        return path( $etc, 'menu.yml');
    },
);

has 'new_item' => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return {
            label     => $self->name,
            name      => $self->name,
            underline => 0,
            sep       => 'none',
            key       => undef,
        };
    },
);

sub menu_updated {
    my $self = shift;
    my $name = $self->name;

    my $yaml;
    try {
        $yaml  = Tpda3::Utils->read_yaml( $self->menu_file_path->stringify );
    }
    catch {
        die "[E] Failed to read the menu configuration file: ",
            $self->menu_file_path, "\n";
    };
    my $menus = Tpda3::Utils->sort_hash_by_id( $yaml->{appmenubar} );

    # Check id the name already exists
    my $name_exists = 0;
    foreach my $menu ( @{$menus} ) {
        my $popup = $yaml->{appmenubar}{$menu}{popup};
        my $knum  = 0;
        foreach my $idx ( keys %{$popup} ) {
            if ($popup->{$idx}{name} eq $name) {
                $name_exists = 1;
            }                               # exists?, aborting
            $knum++;
        }
    }

    # Use an AoH... structure to preserve the order of the items
    my @menu_stru;
    foreach my $menu ( @{$menus} ) {
        my $popup = $yaml->{appmenubar}{$menu}{popup};
        my $popups = {};
        my $pid_cnt = 1;
        foreach my $pid ( sort keys %{$popup} ) {
            $popups->{$pid} = {
                label     => $popup->{$pid}{label},
                name      => $popup->{$pid}{name},
                underline => $popup->{$pid}{underline},
                sep       => $popup->{$pid}{sep},
                key       => $popup->{$pid}{key},
            };
            $pid_cnt++;
        }

        # Insert the new popup name and label
        unless ( $name_exists ) {
            if ($menu eq 'menu_user') {
                $popups->{$pid_cnt} = $self->new_item;
            }
        }

        # Add the popup to the menu
        push @menu_stru, { $menu => {
            id        => $yaml->{appmenubar}{$menu}{id},
            label     => $yaml->{appmenubar}{$menu}{label},
            underline => $yaml->{appmenubar}{$menu}{underline},
            popup     => $popups,
        } };
    }

    return \@menu_stru;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 INTERFACE

=head2 ATTRIBUTES

=head3 name

=head3 info

=head3 menu_file_path

=head3 new_item

=head2 INSTANCE METHODS

=head3 menu_updated

=cut

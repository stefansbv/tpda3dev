package App::Tpda3Dev::Info;

# ABSTRACT: A Tpda3 application info colector

use 5.010001;
use Path::Tiny;
use Moose;
use Locale::TextDomain qw(App-Tpda3Dev);
use App::Tpda3Dev::X qw(hurl);
use App::Tpda3Dev::Info::Db;
use App::Tpda3Dev::Info::Dist;
use App::Tpda3Dev::Info::Config;
use App::Tpda3Dev::Target;
use namespace::autoclean;

has 'main_table' => (
    is  => 'ro',
    isa => 'Str',
);

has 'dep_tables' => (
    is  => 'ro',
    isa => 'Maybe[ArrayRef]',
);

has 'debug' => (
    is  => 'ro',
    isa => 'Bool',
);

#--

has 'config' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info::Config',
    lazy    => 1,
    default => sub {
        return App::Tpda3Dev::Info::Config->new;
    },
);

has 'dist' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info::Dist',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $dist = App::Tpda3Dev::Info::Dist->new;
        $dist->find_app_name;                # init
        return $dist;
    },
);

has 'conn_yaml_file' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $etc = $self->dist->dist_path_for('etc');
        return path( $etc, 'connection.yml');
    },
);

has 'db' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info::Db',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return App::Tpda3Dev::Info::Db->new(
            conn_yaml_file => $self->conn_yaml_file,
            main_table     => $self->main_table,
            dep_tables     => $self->dep_tables,
            debug          => $self->debug,
        );
    },
    handles => [qw(info_conn target)],
);

sub table_columns {
    my ($self, $table) = @_;
    my $engine = $self->target->engine;
    if ( $engine->table_exists($table) ) {
        return $engine->get_columns($table);
    }
    else {
        hurl info =>
            __x( "The '{table}' table does not exists.", table => $table );
    }
}

sub table_keys {
    my ($self, $table) = @_;
    my $engine = $self->target->engine;
    if ( $engine->table_exists($table) ) {
        my $keys = {};
        $keys->{pk} = $engine->table_keys($table);
        $keys->{fk} = $engine->table_keys($table, 1);
        return $keys;
    }
    else {
        hurl info =>
            __x( "The '{table}' table does not exists.", table => $table );
    }
}

sub BUILD {
    my $self = shift;

    # Check if the default mnemonic exists
    my $etc = $self->dist->dist_path_for('etc');
    unless ( $etc->is_dir ) {
        $self->dist->find_mnemonic;    # find and update the mnemonic
    }

    return 1;
};

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 INTERFACE

=head2 ATTRIBUTES

=head3 'main_table'

=head3 'dep_tables'

=head3 'config'

=head3 'dist'

=head3 'conn_yaml_file'

=head3 'db'

=head2 INSTANCE METHODS

=head3 table_columns

=head3 table_keys

=cut

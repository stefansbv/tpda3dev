package App::Tpda3Dev::Command::Columns;

# ABSTRACT: Create a columns.conf file for the transfer app

use 5.010001;
use utf8;
use Path::Tiny;
use File::Copy;
use Try::Tiny;
use Moose::Util::TypeConstraints;
use MooseX::App::Command;
use App::Tpda3Dev::Info;
use App::Tpda3Dev::Render::Conf::Columns;
use namespace::autoclean;

extends qw(App::Tpda3Dev);

command_long_description q[Create a columns.conf file for the transfer application.];

parameter 'action' => (
    is            => 'rw',
    isa           => enum( [qw(new)] ),
    required      => 1,
    documentation => q[Action name ( new ).],
);

parameter 'table' => (
    is            => 'rw',
    isa           => 'Str',
    required      => 1,
    documentation => q[The table name.],
);

has 'info' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $info = App::Tpda3Dev::Info->new(
            main_table => $self->table,
        );
        $info->dist->find_app_name;                # init
        return $info;
    },
);

sub run {
    my $self = shift;
    my $table = $self->table;
    if ( $self->action eq 'new' ) {
        $self->generate_columns;
    }
    else {
        die "Unknown command action!";
    }
    return;
}

sub generate_columns {
    my $self = shift;
    my $table = $self->table;
    my $meta  = $self->info->db->maintable_info;
    my $table_data = $meta->{colinfo};
    print "\nGenerating columns.conf for '$table' ... \r";
    App::Tpda3Dev::Render::Conf::Columns->new(
        info       => $self->info,
        table      => $self->table,
        table_data => $table_data,
    )->render;
    print "\nGenerating columns.conf for '$table' ... done\n";
    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 DESCRIPTION

The implementation of the C<generate> command.

=head1 INTERFACE

=head3 info

=head3 module

=head2 INSTANCE METHODS

=head3 run

The method to be called when the C<generate> command is run.

=head3 generate_columns

=cut

package App::Tpda3Dev::Command::Screen;

# ABSTRACT: Create/update a screen module and config

use 5.010001;
use utf8;
use Path::Tiny;
use File::Copy;
use Try::Tiny;
use Moose::Util::TypeConstraints;
use MooseX::App::Command;
use App::Tpda3Dev::Info;
use App::Tpda3Dev::Render::Screen::Config;
use App::Tpda3Dev::Render::Screen::Module;
use App::Tpda3Dev::Edit::Menu;
use App::Tpda3Dev::Render::Menu;
use namespace::autoclean;

use Data::Dump qw/dump/;

extends qw(App::Tpda3Dev);

command_long_description q[Create or update a screen module and the related configs.];

parameter 'action' => (
    is            => 'rw',
    isa           => enum( [qw(new update)] ),
    required      => 1,
    documentation => q[Action name ( new | update ).],
);

parameter 'screen' => (
    is            => 'rw',
    isa           => 'Str',
    required      => 1,
    documentation => q[The screen module name.],
);

parameter 'main_table' => (
    is            => 'rw',
    isa           => 'Str',
    required      => 1,
    documentation => q[The main table name.],
);

option 'table' => (
    is            => 'rw',
    isa           => 'Str',
    documentation => q[The dependent table name(s).],
);

option 'debug' => (
    is            => 'ro',
    isa           => 'Bool',
    documentation => q[The Debug option.],
    default       => sub { 0 },
);

has 'module' => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return $self->info->dist->module;
    },
);

has 'info' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info',
    lazy    => 1,
    default => sub {
        my $self   = shift;
        my $para   = $self->table;
        my $tables = $self->tables_aref($para);
        if ( $self->debug ) {
            say "para = $para\n";
            say "dep tables:\n";
            dump $tables;
            say "---\n";
        }
        my $info   = App::Tpda3Dev::Info->new(
            main_table => $self->main_table,
            dep_tables => $tables,
            debug      => $self->debug,
        );
        $info->dist->find_app_name;    # init
        return $info;
    },
);

sub tables_aref {
    my ($self, $para) = @_;
    my $tables = [];
    if ( defined $para ) {
        if ( $para =~ m/[,]/ ) {
            $tables = [ map { $_ } split ',', $para ];
        }
        else {
            $tables = [$para];
        }
    }
    return $tables;
}

sub run {
    my $self = shift;

    if ( $self->debug ) {
        say "---\n";
        say "params:\n";
        say "action = ", $self->action;
        say "screen = ", $self->screen;
        say "table  = ", $self->table;
        say "---\n";
    }

    my $module = $self->module;
    print "\nModule name: $module\n" if $self->verbose;
    my $screen = $self->screen;
    print "Screen name: $screen\n" if $self->verbose;

    if ( $self->action eq 'new' ) {
        print "\nCreating screen '$screen' for '$module' ... \r" unless $self->verbose;
        $self->generate_config;
        $self->generate_screen;
        print "\nCreating screen '$screen' for '$module' ... done\n";
        $self->update_menu;
    }
    elsif ( $self->action eq 'update' ) {
        print "\nUpdate screen: $screen\n" if $self->verbose;
        $self->generate_screen;
    }
    else {
        die "Unknown command action!";
    }
    return;
}

sub generate_config {
    my $self = shift;
    my $screen = $self->screen;
    if ( $self->debug ) {
        my $d_data = $self->info->db->deptables_data;
        print "deptables data\n";
        dump $d_data;
        print "---\n\n";
    }
    print "\nGenerating config for '$screen' ... \r";
    App::Tpda3Dev::Render::Screen::Config->new(
        info           => $self->info,
        screen         => $self->screen,
        maintable_data => $self->info->db->maintable_data,
        deptables_data => $self->info->db->deptables_data,
    )->render;
    print "\nGenerating config for '$screen' ... done\n";
    return;
}

sub generate_screen {
    my $self   = shift;
    my $screen = $self->screen;
    print "\nGenerating screen '$screen' ... \r";
    my $ren = App::Tpda3Dev::Render::Screen::Module->new(
        info        => $self->info,
        module      => $self->module,
        screen      => $self->screen,
        output_file => "$screen.pm",
    )->render;
    print "\nGenerating screen '$screen' ... done\n";
    return;
}

sub update_menu {
    my $self = shift;
    my $menu_data = App::Tpda3Dev::Edit::Menu->new(
        info => $self->info,
        name => $self->screen,
    )->menu_updated;
    App::Tpda3Dev::Render::Menu->new(
        info     => $self->info,
        menudata => $menu_data,
    )->render;
    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 DESCRIPTION

The implementation of the C<generate> command.

=head1 INTERFACE

=head3 info

=head3 module

=head2 INSTANCE METHODS

=head3 tables_aref

Turn a string into an array reference.

=head3 run

The method to be called when the C<generate> command is run.

=head3 generate_config

=head3 generate_screen

=head3 update_menu

=head3 update_screen

=cut

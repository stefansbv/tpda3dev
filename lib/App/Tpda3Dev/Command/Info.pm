package App::Tpda3Dev::Command::Info;

# ABSTRACT: Print miscellaneous info

use 5.010001;
use utf8;
use Try::Tiny;
use Path::Tiny;
use Moose::Util::TypeConstraints;
use MooseX::App::Command;
use Locale::TextDomain qw(App-Tpda3Dev);
use App::Tpda3Dev::X qw(hurl);
use App::Tpda3Dev::Info;
use namespace::autoclean;

extends qw(App::Tpda3Dev);

command_long_description q[Print miscellaneous info.];

parameter 'action' => (
    is            => 'rw',
    isa           => enum( [qw(dist table mnemo screen)] ),
    required      => 1,
    documentation => q[Action name (dist|table|mnemo|screen).],
);

parameter 'detail' => (
    is            => 'rw',
    isa           => 'Str',
    documentation => q[The detail parameter. Can be a table name or a mnemonic.],
);

has 'info' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $info = App::Tpda3Dev::Info->new;
        $info->dist->find_app_name;                # init
        return $info;
    },
);

sub run {
    my ($self) = @_;

    my $action = $self->action;
SWITCH: {
        $action eq 'dist'   && do { $self->get_dist_info; last SWITCH; };
        $action eq 'table'  && do { $self->get_table_info; last SWITCH; };
        $action eq 'mnemo'  && do { $self->get_mnemonic_info; last SWITCH; };
        $action eq 'screen' && do { $self->get_screen_info; last SWITCH; };
        print "The \$action is not within valid options\n";
    }

    return;
}

sub get_dist_info {
    my $self = shift;
    my $info = try { $self->info->dist }
    catch {
        say " $_";
        return undef;
    };
    if ($info) {
        my $scope = 'create' if $info->context eq 'new';
        $scope = 'update' if $info->context eq 'update';

        say ' Current distribution: ', $info->find_app_name;
        say '             mnemonic: ', $info->mnemonic;
        say '        current scope: ', $scope;
        say '    distribution name: ', $info->dist_name;
        say '     user config path: ', $info->configdir;
        say '             user etc: ', $info->user_path_for('etc'),
            $info->check( $info->exists_user_path_for('etc') );
        say '             user scr: ', $info->user_path_for('scr'),
            $info->check( $info->exists_user_path_for('scr') );
        say '             user rep: ', $info->user_path_for('rep'),
            $info->check( $info->exists_user_path_for('rep') );
        say '             user res: ', $info->user_path_for('res'),
            $info->check( $info->exists_user_path_for('res') );
        say '   screen module path: ', $info->screen_module_path;
        say ' Screens:';
        $info->list_screen_modules;
    }
    return;
}

sub get_table_info {
    my $self = shift;
    $self->detail
        ? $self->print_table_columns
        : $self->print_table_list;
    return;
}

sub get_mnemonic_info {
    my $self = shift;
    my $mnemonic = $self->detail;
    say "# mnemonic = $mnemonic";
    $self->info->config->tpda->list_mnemonics($mnemonic);
    return;
}

sub get_screen_info {
    my $self = shift;
    $self->info->dist->list_config_files;
    return;
}

sub _table_list {
    my $self   = shift;
    my $engine = $self->info->target->engine;
    return $engine->table_list(undef, 'with_schema');
}

sub print_table_list {
    my $self = shift;
    my $output = "\nTables:\n - ";
    $output .= join "\n - ", sort @{ $self->_table_list };
    $output .= "\n";
    print $output;
    print "\n";
}

sub print_table_columns {
    my $self = shift;
    my $table = $self->detail;
    my $output = "\nColumns [$table]:\n - ";
    $output .= join "\n - ", sort @{ $self->info->table_columns($table) };
    $output .= "\n";
    $output .= "\nKeys [$table]:";
    $output .= "\n primary\n - ";
    $output .= join "\n - ", @{ $self->info->table_keys($table)->{pk} };
    $output .= "\n foreign\n - ";
    $output .= join "\n - ", @{ $self->info->table_keys($table)->{fk} };
    $output .= "\n";
    print $output;
    print "\n";
    return;
}

__PACKAGE__->meta->make_immutable;

1;
__END__

=encoding utf8

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 INTERFACE

=head2 ATTRIBUTES

=head3 dist

=head3 config

=head3 conn_yaml_file

=head3 db

=head3 target

=head2 INSTANCE METHODS

=head3 run

The method to be called when the C<info> command is run.

=head3 run

=head3 get_dist_info

=head3 get_table_info

=head3 get_mnemonic_info

=head3 get_screen_info

=head3 _table_list

=head3 print_table_list

=head3 _table_columns

=head3 print_table_columns

=cut

package App::Tpda3Dev::Command::Create;

# ABSTRACT: Create a new Tpda3 application project

use 5.010001;
use utf8;
use Path::Tiny;
use File::Copy;
use File::Copy::Recursive qw(dircopy);
use Try::Tiny;
use MooseX::App::Command;
use namespace::autoclean;

extends qw(App::Tpda3Dev);

use App::Tpda3Dev::Info::Dist;
use App::Tpda3Dev::Render::Module;
use App::Tpda3Dev::Render::Makefile;
use App::Tpda3Dev::Render::Readme;
use App::Tpda3Dev::Render::YAML;
use App::Tpda3Dev::Render::Test;

command_long_description q[Create a new Tpda3 project.];

parameter 'module' => (
    is            => 'rw',
    isa           => 'Str',
    required      => 1,
    documentation => q[The main module name of the application.],
);

option 'uri' => (
    is            => 'rw',
    isa           => 'Str',
    cmd_flag      => 'uri',
    required      => 1,
    documentation => q[The URI to connect to the database.],
);

option 'dzil' => (
    is            => 'rw',
    isa           => 'Bool',
    cmd_flag      => 'dzil',
    documentation => q[Configure for Dist::Zilla.],
);

option 'path' => (
    is            => 'rw',
    isa           => 'Str',
    cmd_flag      => 'path',
    documentation => q[The path to the new dist.],
);

has 'dist_path' => (
    is      => 'ro',
    isa     => 'Path::Tiny',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->path ? path( $self->path ) : path('.');
    },
);

#---

has 'mnemonic' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return lc $self->module;;
    },
);

has 'info' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info::Dist',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return App::Tpda3Dev::Info::Dist->new(
            dist_path => $self->dist_path,
            module    => $self->module,
        );
    },
);

sub run {
    my ( $self ) = @_;

    # die "Wrong context!\n",
    #     "The 'create' command must NOT be run from a Tpda3 application directory.\n"
    #     unless $self->cache->get('context') eq 'new';

    $self->make_app_tree;

    return;
}

sub make_app_tree {
    my ( $self, $args ) = @_;

    my $module   = $self->module;
    my $mnemonic = $self->mnemonic;

    print "\nCreating distribution for '$module' ... \r";

    my $moduledir = $self->info->relative_path_to_dist(
        $self->info->module_path,
    );
    die "The distribution path '$moduledir' already exists, aborted."
        if -d $moduledir;
    Tpda3::Config::Utils->create_path($moduledir);

    # Populate module dir

    $File::Copy::Recursive::KeepMode = 0;    # mode 644

    my $module_src = $self->info->module_src;
    my $distdir    = $self->info->relative_path_to_dist('.');
    dircopy( $module_src, $distdir )
        or die "Failed to copy module tree to '$moduledir'";

    # Create dist config path
    my $configdir = path $self->info->relative_path_to_dist(
        $self->info->dist_sharedir ), $mnemonic;
    Tpda3::Config::Utils->create_path($configdir);

    # Populate config dir
    my $config_src = $self->info->config_src;
    dircopy( $config_src, $configdir )
        or die "Failed to copy module tree to '$configdir'";

    # Populate inc dir
    unless ( $self->dzil ) {
        my $dirtree_inc    = path $self->info->dirtree, 'inc';
        my $moduledir_inc  = path $distdir, 'inc';
        dircopy( $dirtree_inc, $moduledir_inc )
            or die "Failed to copy inc tree '$dirtree_inc' to '$moduledir_inc'";
    }

    App::Tpda3Dev::Render::Module->new(
        module      => $module,
        output_path => $moduledir,
    )->render;

    # Create screens modules dir
    my $screendir = $self->info->relative_path_to_dist(
        $self->info->screen_module_path,
    );
    Tpda3::Config::Utils->create_path($screendir);

    # Make Makefile.PL script                                # use Dist::Zilla?
    App::Tpda3Dev::Render::Makefile->new(
        module      => $module,
        output_path => $distdir,
    )->render;

    # Make README file                                       # use Dist::Zilla?
    App::Tpda3Dev::Render::Readme->new(
        module      => $module,
        output_path => $distdir,
    )->render;

    # Make etc/*.yml configs
    my $etcdir = $self->info->relative_path_to_dist(
        $self->info->dist_path_for('etc'),
    );
    my $conf = [
        [ 'cfg-application', 'application.yml' ],
        [ 'cfg-menu',        'menu.yml' ],
        [ 'cfg-connection',  'connection.yml' ],
    ];
    foreach my $pair ( @{$conf} ) {
        # say "create $pair->[1]";
        App::Tpda3Dev::Render::YAML->new(
            module      => $module,
            mnemonic    => $mnemonic,
            uri         => $self->uri,
            templ_type  => $pair->[0],
            output_file => $pair->[1],
            output_path => $etcdir,
        )->render;
    }

    # Make t/*.t test scripts
    my $testdir = $self->info->relative_path_to_dist(
        $self->info->testdir,
    );
    my $test = [
        [ 'test-load',       '00-load.t' ],
        [ 'test-config',     '10-config.t' ],
        [ 'test-connection', '20-connection.t' ],
    ];
    foreach my $pair ( @{$test} ) {
        # say "create $pair->[1]";
        App::Tpda3Dev::Render::Test->new(
            module      => $module,
            mnemonic    => $mnemonic,
            templ_type  => $pair->[0],
            output_file => $pair->[1],
            output_path => $testdir,
        )->render;
    }

    print "Creating distribution for '$module' ... done\n";

    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 Description

The implementation of the C<create> command.

=head1 Interface

=head2 Attributes

=head2 Instance Methods

=head3 run

The method to be called when the C<create> command is run.

=head3 make_app_tree

Create a Tpda3 distribution tree.

=cut

package App::Tpda3Dev::Command::PgTap;

# ABSTRACT: Create pgTAP test

use 5.010001;
use utf8;
use Path::Tiny;
use File::Copy;
use Try::Tiny;
use Moose::Util::TypeConstraints;
use MooseX::App::Command;
use App::Tpda3Dev::Info;
use App::Tpda3Dev::Render::PgTap;
use namespace::autoclean;

extends qw(App::Tpda3Dev);

command_long_description q[Create a SQL file with pgTAP tests.];

parameter 'action' => (
    is            => 'rw',
    isa           => enum( [qw(new)] ),
    required      => 1,
    documentation => q[Action name ( new ).],
);

parameter 'table' => (
    is            => 'rw',
    isa           => 'Str',
    required      => 1,
    documentation => q[The table name.],
);

has 'info' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $info = App::Tpda3Dev::Info->new(
            main_table => $self->table,
        );
        $info->dist->find_app_name;                # init
        return $info;
    },
);

sub run {
    my $self = shift;
    my $table = $self->table;
    if ( $self->action eq 'new' ) {
        $self->generate_pgtap_sql;
    }
    else {
        die "Unknown command action!";
    }
    return;
}

sub generate_pgtap_sql {
    my $self = shift;
    my $table = $self->table;
    my $meta  = $self->info->db->maintable_info;
    my $table_data = $meta->{colinfo};
    #print "\nGenerating pgTAP tests for '$table'... \r";
    App::Tpda3Dev::Render::PgTap->new(
        table      => $self->table,
        table_data => $table_data,
    )->render;
    print "\nGenerating pgTAP tests for '$table'... done\n";
    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 DESCRIPTION

The implementation of the C<pgtap> command.

=head1 INTERFACE

=head3 info

=head3 module

=head2 INSTANCE METHODS

=head3 run

The method to be called when the C<pgtap> command is run.

=head3 generate_pgtap_sql

=cut

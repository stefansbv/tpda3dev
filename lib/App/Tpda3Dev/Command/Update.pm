package App::Tpda3Dev::Command::Update;

# ABSTRACT: Update/install user configuration files

use 5.010001;
use utf8;
use Try::Tiny;
use Path::Tiny;
use Moose::Util::TypeConstraints;
use MooseX::App::Command;
use Locale::TextDomain qw(App-Tpda3Dev);
use App::Tpda3Dev::X qw(hurl);
use App::Tpda3Dev::Info;
use App::Tpda3Dev::Utils;
use namespace::autoclean;

extends qw(App::Tpda3Dev);

command_long_description q[Update/install user configuration files.];

parameter 'what' => (
    is            => 'rw',
    isa           => enum( [qw(all etc rep res scr tex sys)] ),
    required      => 1,
    documentation => q[What to update (all|etc|rep|res|scr|tex|sys).],
);

option 'src' => (
    is            => 'rw',
    isa           => 'Str',
    documentation => q[Mnemonic sursa.],
);

option 'dst' => (
    is            => 'rw',
    isa           => 'Str',
    documentation => q[Mnemonic destinatie.],
);

has 'info' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Info',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $info = App::Tpda3Dev::Info->new;
        $info->dist->find_app_name;                # init
        return $info;
    },
);

has 'util' => (
    is      => 'ro',
    isa     => 'App::Tpda3Dev::Utils',
    lazy    => 1,
    default => sub {
        return App::Tpda3Dev::Utils->new;
    },
);

sub run {
    my ($self) = @_;
    my $what = $self->what;
  SWITCH: {
        $what eq 'etc' && do { $self->update_what('etc'); last SWITCH; };
        $what eq 'rep' && do { $self->update_what('rep'); last SWITCH; };
        $what eq 'res' && do { $self->update_what('res'); last SWITCH; };
        $what eq 'scr' && do { $self->update_what('scr'); last SWITCH; };
        $what eq 'tex' && do { $self->update_what('tex'); last SWITCH; };
        $what eq 'all' && do { $self->update_all;         last SWITCH; };
        $what eq 'sys' && do { $self->update_system;      last SWITCH; };
        print "The \$what is not within valid options\n";
    }
    return;
}

sub update_all {
    my $self = shift;
    $self->update_what('etc');
    $self->update_what('scr');
    $self->update_what('res');
    $self->update_what('rep');
    $self->update_what('tex');
}

sub update_what {
    my ($self, $what) = @_;
    my $info = try { $self->info->dist }
    catch {
        print " $_\n";
        return undef;
    };
    return unless $info;

    my $src = $self->src;
    my $dst = $self->dst;
    my $src_path = $info->dist_path_for($what, $src);
    my $dst_path = $info->user_path_for($what, $dst);

    say "Check '$what' path:";
    say ' dist path: ', $src_path,
        $info->check( $info->exists_dist_path_for($what) );
    say ' user path: ', $dst_path,
        $info->check( $info->exists_user_path_for($what) );

    $self->_update($what, $src_path, $dst_path);

    return;
}

sub update_system {
    my $self = shift;

    my $info = try { $self->info->dist }
    catch {
        print "This command must be run from the tpda3 dist dir.\n$_\n";
        return undef;
    };
    return unless $info;

    my $src = $self->src;
    my $dst = $self->dst;

    foreach my $what ( (qw{scr}) ) {
        my $src_path = $info->dist_sys_path_for( $what, $src );
        my $dst_path = $info->user_sys_path_for( $what, $dst );

        say "Check '$what' path:";
        say ' dist path: ', $src_path,
            $info->check( $info->exists_dist_sys_path_for($what) );
        say ' user path: ', $dst_path,
            $info->check( $info->exists_user_sys_path_for($what) );

        $self->_update( $what, $src_path, $dst_path );
    }

    return;
}

sub _update {
    my ($self, $what, $src_path, $dst_path) = @_;
    my $files = $self->util->file_list($src_path);
    if ( scalar @{$files} == 0 ) {
        say __x "[II] No files to install or update from '{path}'",
          path => $src_path;
    }
    else {
        say "[II] Install or update user '$what' config";
        foreach my $file ( @{$files} ) {
            next if $file->is_dir;
            if ( $file->is_file ) {
                my $rel = path($file)->relative($src_path);
                $self->copy_if_not_selfsame( $src_path, $dst_path, $rel );
            }
        }
    }
    say "";
    return;
}

sub copy_if_not_selfsame {
    my ($self, $src_path, $dst_path, $file_name) = @_;
    my $src = path $src_path, $file_name;
    my $dst = path $dst_path, $file_name;
    my $rel_path = path($file_name)->parent;
    $self->check_dir( path($dst_path, $rel_path) );
    my $verb = 'update';
    if ( $dst->is_file ) {
        if ( $self->util->is_selfsame($src, $dst) ) {
            $verb = 'skipped';
        }
        else {
            $verb = 'install';
        }
    }
    if ( $verb eq 'skipped' ) {
        print " $verb $src" if $self->verbose;
        $self->util->set_perm( $dst, '0444' )
            if $dst->is_file
            and $self->util->get_perms($dst) ne '0444';    # make $dst ro
        print "\n" if $self->verbose;
        return;
    }
    else {
        print " $verb $src";
    }
    $self->check_perms($dst);
    $self->util->copy_file_local( $src, $dst );
    $self->util->set_perm( $dst, '0444' )
        if $self->util->get_perms($dst) ne '0444';
    print $self->info->dist->check( $self->util->is_selfsame( $src, $dst ) ),
        "\n";
}

sub check_dir {
    my ( $self, $dir ) = @_;
    $dir = path $dir;
    unless ( $dir->is_dir ) {
        print " make user path: $dir";
        $self->util->make_path($dir);
        print $self->info->dist->check( $dir->is_dir ), "\n";
    }
    return;
}

sub check_perms {
    my ($self, $file) = @_;
    return unless $file->is_file;
    unless ( $self->util->get_perms($file) eq '0644' ) {
        $self->util->set_perm($file, '0644');
    }
    return;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 INTERFACE

=head2 ATTRIBUTES

=head3 info

=head3 util

=head2 INSTANCE METHODS

=head3 run

=head3 update_all

=head3 update_what

=head3 copy_if_not_selfsame

=head3 check_dir

=head3 check_perms

=cut

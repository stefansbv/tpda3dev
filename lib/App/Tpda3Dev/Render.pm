package App::Tpda3Dev::Render;

# ABSTRACT: Render a file from a template

use 5.010001;
use utf8;
use Moose;
use Template;
use File::ShareDir qw(dist_dir);
use Path::Tiny;
use Try::Tiny;

use Tpda3::Utils;

extends qw(App::Tpda3Dev);

has '_tpda_templates' => (
    traits  => ['Hash'],
    is      => 'ro',
    isa     => 'HashRef[Str]',
    default => sub {
        return {
            'config'          => 'config.tt',
            'config-update'   => 'config-refactor.tt',
            'screen'          => 'screen.tt',
            'module'          => 'module.tt',
            'menu'            => 'menu.tt',
            'makefile'        => 'makefile.tt',
            'readme'          => 'readme.tt',
            'cfg-application' => 'config/application.tt',
            'cfg-menu'        => 'config/menu.tt',
            'cfg-connection'  => 'config/connection.tt',
            'test-load'       => 'test/load.tt',
            'test-config'     => 'test/config.tt',
            'test-connection' => 'test/connection.tt',
            'columns-conf'    => 'conf/columns.tt',
            'pgtap-table'     => 'pgtap-table.tt',
        };
    },
    handles => {
        get_template_for => 'get',
    },
);

has 'templ_type' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'templ_data' => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

has 'templ_path' => (
    is       => 'rw',
    isa      => 'Path::Tiny',
    default => sub {
        my $dist_dir = try { dist_dir('App-Tpda3Dev') }
        catch {
            warn "The application is not yet installed.";
            return undef;    # required
        };
        if ($dist_dir) {
            return path $dist_dir, 'templates';
        }
        else {
            return path( 'share', 'templates');
        }
    },
);

has 'output_path' => (
    is       => 'ro',
    isa      => 'Path::Tiny',
    required => 1,
);

has 'output_file' => (
    is       => 'ro',
    isa      => 'Maybe[Str]',
    required => 1,
);

sub render {
    my $self = shift;

    my $templ_type  = $self->templ_type;
    my $output_file = $self->output_file;
    my $template    = $self->get_template_for($templ_type);
    die "No template for '$templ_type'" unless $template;

    # Fallback to local share
    unless ( path($self->templ_path, $template)->is_file ) {
        $self->templ_path( path( 'share', 'templates') );
    }

    #XXX $output_file = "${templ_type}$output_file" if $output_file =~ m{^\.};

    # Avoid UTF-8 problems in TeX
    my $rec = $self->templ_data;
    foreach my $key ( keys %{$rec} ) {
        my $val = $rec->{$key};
        next if !$val || ref $val; # XXX need to descend deeply and fix all values?
        $val = Tpda3::Utils->decode_unless_utf($val);
        $rec->{$key} = $val;
    }

    my $tt = Template->new(
        ENCODING     => 'utf8',
        INCLUDE_PATH => $self->templ_path,
        OUTPUT_PATH  => $self->output_path,
    );
    $tt->process( $template, $rec, $output_file, binmode => ':utf8' )
        or die $tt->error(), "\n";

    return $output_file;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 Synopsis

    package App::Tpda3Dev::Render::Module;
    use Moose;
    extends qw(App::Tpda3Dev::Render);

In an other module, later...

    use App::Tpda3Dev::Render::Module;
    my $ren = App::Tpda3Dev::Render::Module->new(
       templ_type  => 'module',
       templ_data  => { qw{some data} },
       output_path => './',
       output_file => 'MyNewModule',
    );

=head1 Description

A module to render a file from a template.  It is supposed to be subclassed.

=head1 Interface

=head2 Attributes

=head3 _tpda_templates

A required C<HashRef> attribute holding the mapping between a name and
a relative path to a TT template.

The C<get_template_for> method returns the TT template path for any
valid template type name.

=head3 templ_type

A required C<Str> attribute holding the name of the template type.  It
must be one of the keys returned by the C<_tpda_templates> attribute.

_tpda_templates

=head3 templ_data

A required C<HashRef> attribute holding the data to be passes to the
TT template.

=head3 templ_path

A required C<Path::Tiny> attribute holding the path to the template.
If App::Tpda3Dev is not installed properly, returns the
L<./share/templates> path, which may not be in CWD.

=head3 output_path

A required C<Path::Tiny> attribute holding the path where the new file
will be generated.

=head3 output_file

A required C<Str> attribute holding the output file name.

=head2 Instance Methods

=head3 render

Generate a file from templates.

If the output file parameter is just the file extension, then use type
as file name.

=head2 get_template_for

Return the template name for one of the two known types: C<config> or
C<screen>.

=cut

package App::Tpda3Dev::Config;

# ABSTRACT: The configuration module

use 5.010001;
use utf8;
use English;
use Moose;
use File::HomeDir;
use Path::Tiny;
use Config::GitLike 1.11;
use URI;
use namespace::autoclean;

extends 'Config::GitLike';

has '+confname' => ( default => 'tpda3drc' );
has '+encoding' => ( default => 'UTF-8' );

sub dir_file { undef }

override global_file => sub {
    my $self = shift;
    return path $ENV{APP_T3D_SYS_CONFIG}
        || $self->SUPER::global_file(@_);
};

override user_file => sub {
    my $self = shift;
    return path $ENV{APP_T3D_USR_CONFIG}
        || $self->SUPER::user_file(@_);
};

sub get_gitconfig {
    my $self = shift;

    my $config_file = path( File::HomeDir->my_home, '.gitconfig');

    unless ( $config_file->is_file ) {
        print "Git configuration file not found!\n";
        print "Please configure with:\n";
        print "# git config --global user.name 'John Doe'\n";
        print "# git config --global user.email johndoe\@example.com\n";
        return ('<user name here>', '<user e-mail here>');
    }

    my $c = Config::GitLike->new( confname => $config_file->stringify );
    my $user  = $c->get( key => 'user.name' );
    my $email = $c->get( key => 'user.email' );

    return ($user, $email);
}

__PACKAGE__->meta->make_immutable;

1;

__END__

=encoding utf8

=head1 Synopsis

  my $config = App::Tpda3Dev::Config->new;
  say scalar $config->dump;

=head1 Description

This class provides the interface to App::Tpda3Dev configuration.
It inherits from L<Config::GitLike>.

=head1 Interface

=head2 Attributes

=head3 confname

The name of the configuration file.

=head3 encoding

The default encoding is UTF-8.

=head3 global_file

The global config file name can be set using the C<APP_T3D_SYS_CONFIG>
ENV var.

=head3 user_file

The user config file name can be set using the C<APP_T3D_USR_CONFIG>
ENV var.

=head3 get_gitconfig

Get and return the C<user> and C<email> from the user C<gitconfig>
file.  Used for filling in the author section in PODs.

=head2 Instance Methods

=cut

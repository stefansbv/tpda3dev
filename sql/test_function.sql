/*
 *
 * from: http://computerexpress2.blogspot.com/2017/11/testing-stored-functions-using-pgtap.html
 */

CREATE OR REPLACE FUNCTION test.test_user_0010()
RETURNS SETOF TEXT AS $$
--A room in the name of f-s is optional, but may be useful to run the test f th
--in a certain order when running tests with runtests().

--Declare a variable which will store the ID of the added record:
DECLARE
v_user_id INTEGER;
BEGIN
-- First test - check that the feature works without exceptions:
RETURN NEXT lives_ok('select test_user.add_user("testuser unique"::varchar);',
'test_user.add_user doesnt throw exception');

-- Add another record, save the ID, verify it is correct (>0):
v_user_id := test_user.add_user('blah blah');
RETURN NEXT cmp_ok(v_user_id,'>',0,'test_user.add_user: returns 'ok');

-- Check that the record really is.
RETURN NEXT results_eq('select user_name::varchar from test_user.users where user_id=' || v_user_id::varchar,
'SELECT "blah blah"::varchar','test_user.add_user inserts ok');

-- Function change the user should return the user ID:
RETURN NEXT is(test_user.alter_user(v_user_id,'new user name blah'),
v_user_id,'test_user.alter_user: returns 'ok');

-- Check that a record has really changed:
RETURN NEXT results_eq('select user_name::varchar from test_user.users where user_id=' || v_user_id::varchar,
'select "new user name blah"::varchar',
'test_user.alter_user record updates');

-- The function of deleting a user should return its id:
RETURN NEXT is(test_user.delete_user(v_user_id),
v_user_id,
'test_user.delete_user: returns 'ok');

-- The last test. Check that the user is really deleted:
RETURN NEXT is_empty('select 1 from test_user.users where user_id=' || v_user_id::varchar,
'test_user.delete_user: deletes ok');

$$ language plpgsql;

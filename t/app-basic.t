#
# Test the application commands, parameters and options
#
use 5.010;
use utf8;
use Test::Most;
use Path::Tiny;

use App::Tpda3Dev;
use App::Tpda3Dev::Info::Dist;

ok my $info = App::Tpda3Dev::Info::Dist->new(
    dist_path => path( qw(t output) ),
), 'new dist info';
ok my $configdir = $info->configdir;
my $uri = "sqlite:$configdir/classicmodels.db"; # should exists if
                                                # Tpda3 is properly
                                                # tested and installed

subtest 'Command "create" with full options' => sub {
    MooseX::App::ParsedArgv->new( argv => [qw(create test --verbose), "--uri=$uri"] );
    my $chk_01 = App::Tpda3Dev->new_with_command();
    isa_ok $chk_01, 'App::Tpda3Dev::Command::Create';
    is $chk_01->dryrun, undef, 'option "dryrun" is not set';
    is $chk_01->verbose, 1, 'option "verbose" is set';
    is $chk_01->uri, $uri, 'option "uri" is set';
};

done_testing;

use 5.010001;
use strict;
use warnings;
use Path::Tiny;
use Test::More;
use Test::Exception;

use App::Tpda3Dev::Render::Conf::Columns;
use Tpda3::Config::Utils;

my $output_file = 'columns.conf';
my $output_path = path( 't', 'output' );

my $table   = 'orders';
my $screen  = ucfirst $table;
my $table_meta = {
    comments => {
        defa        => undef,
        is_nullable => 1,
        length      => undef,
        name        => "comments",
        pos         => 5,
        prec        => undef,
        scale       => undef,
        type        => "TEXT",
    },
    customernumber => {
        defa        => undef,
        is_nullable => 0,
        length      => undef,
        name        => "customernumber",
        pos         => 6,
        prec        => undef,
        scale       => undef,
        type        => "SMALLINT",
    },
    orderdate => {
        defa        => undef,
        is_nullable => 0,
        length      => undef,
        name        => "orderdate",
        pos         => 1,
        prec        => undef,
        scale       => undef,
        type        => "DATE",
    },
    ordernumber => {
        defa        => undef,
        is_nullable => 1,
        length      => undef,
        name        => "ordernumber",
        pos         => 0,
        prec        => undef,
        scale       => undef,
        type        => "INTEGER",
    },
    ordertotal => {
        defa        => "0.00",
        is_nullable => 1,
        length      => 10,
        name        => "ordertotal",
        pos         => 7,
        prec        => 10,
        scale       => 2,
        type        => "DECIMAL",
    },
    requireddate => {
        defa        => undef,
        is_nullable => 0,
        length      => undef,
        name        => "requireddate",
        pos         => 2,
        prec        => undef,
        scale       => undef,
        type        => "DATE",
    },
    shippeddate => {
        defa        => "NULL",
        is_nullable => 1,
        length      => undef,
        name        => "shippeddate",
        pos         => 3,
        prec        => undef,
        scale       => undef,
        type        => "DATE",
    },
    statuscode => {
        defa        => "NULL",
        is_nullable => 1,
        length      => 1,
        name        => "statuscode",
        pos         => 4,
        prec        => 1,
        scale       => undef,
        type        => "CHAR",
    },
};

ok my $ren = App::Tpda3Dev::Render::Conf::Columns->new(
    table_data  => $table_meta,
    templ_path  => path( 'share', 'templates' ),
    output_file => $output_file,
    output_path => $output_path,
), 'new render config';
is $ren->get_template_for('columns-conf'), 'conf/columns.tt', 'template for conf';
is ref $ren->templ_data, 'HASH', 'template data';
ok $ren->render, 'render conf file';

my $conf_cols = path $output_path, $output_file;
lives_ok { Tpda3::Config::Utils->load_conf( $conf_cols->stringify ) }
    'conf load';

done_testing;

# Cleanup
END {
    unlink $conf_cols or warn $!;
}

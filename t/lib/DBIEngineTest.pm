package DBIEngineTest;
#
# Adapted from Sqitch by theory.
#
# Changed to to use only ASCII because of:
# Wide character in print at lib/site_perl/5.14.4/Test/Builder.pm line 1826.
# when is_deeply reports failure
#
use 5.010;
use strict;
use warnings;
use utf8;
use Try::Tiny;
use Test::Most;
use Test::MockModule;
use Path::Class 0.33 qw(file dir);
use Locale::TextDomain qw(App-Tpda3Dev);

# Just die on warnings.
use Carp; BEGIN { $SIG{__WARN__} = \&Carp::confess }

sub run {
    my ( $self, %p ) = @_;

    my $class = $p{class};

    can_ok $class, qw(
        get_info
        table_exists
    );

    subtest 'live database' => sub {

        ok my $target = App::Tpda3Dev::Target->new(
            @{ $p{target_params} || [] },
        ), 'new target';
        isa_ok $target, 'App::Tpda3Dev::Target', 'target';

        ok my $engine = $class->new(
            target   => $target,
            @{ $p{engine_params} || [] },
        ), 'new engine';
        if (my $code = $p{skip_unless}) {
            try {
                $code->( $engine ) || die 'NO';
            } catch {
                plan skip_all => sprintf(
                    'Unable to live-test %s engine: %s',
                    $class->name,
                    eval { $_->message } || $_
                );
            };
        }

        ok $engine, 'Engine instantiated';

        throws_ok { $engine->dbh->do('INSERT blah INTO __bar_____') }
            'App::Tpda3Dev::X',
            'Database error should be converted to Tpda3Dev exception';
        is $@->ident, $class->key, 'Ident should be the engine';
        ok $@->message, 'The message should be from the translation';


        #######################################################################
        # Test the database connection, if appropriate.
        if ( my $code = $p{test_dbh} ) {
            $code->( $engine->dbh );
        }


        #######################################################################

        # Test begin_work() and finish_work().
        can_ok $engine, qw(begin_work finish_work);
        my $mock_dbh
            = Test::MockModule->new( ref $engine->dbh, no_auto => 1 );
        my $txn;
        $mock_dbh->mock( begin_work => sub { $txn = 1 } );
        $mock_dbh->mock( commit     => sub { $txn = 0 } );
        $mock_dbh->mock( rollback   => sub { $txn = -1 } );
        my @do;
        $mock_dbh->mock(
            do => sub {
                shift;
                @do = @_;
            }
        );
        ok $engine->begin_work, 'Begin work';
        is $txn, 1, 'Should have started a transaction';
        ok $engine->finish_work, 'Finish work';
        is $txn, 0, 'Should have committed a transaction';
        ok $engine->begin_work, 'Begin work again';
        is $txn, 1, 'Should have started another transaction';
        ok $engine->rollback_work, 'Rollback work';
        is $txn, -1, 'Should have rolled back a transaction';
        $mock_dbh->unmock('do');

        isa_ok $engine, 'App::Tpda3Dev::Engine', 'Engine';
        like $engine->key, qr/pg|firebird|sqlite/, 'the key attribute should be set';
        like $engine->name, qr/PostgreSQL|Firebird|SQLite/,
            'the name attribute should be set';
        like $engine->driver, qr/DBD::Pg|DBD::Firebird|DBD::SQLite/,
            'the driver attribute should be set';


        ######################################################################
        # Test someting specific for Pg

        if ($class eq 'App::Tpda3Dev::Engine::pg') {
            my ($sch, $tbl) = $engine->get_schema_name('schema_name.table_name');
            is $sch, 'schema_name', 'schema';
            is $tbl, 'table_name', 'table';

            ($sch, $tbl) = $engine->get_schema_name('table_name');
            is $sch, undef, 'schema';
            is $tbl, 'table_name', 'table';
        }


        ######################################################################
        # Test the engine methods

        my @fields_info = (
            [ 'field_00', 'integer' ],
            [ 'field_01', 'character(1)' ],
            [ 'field_02', 'date' ],
            [ 'field_03', 'integer' ],
            [ 'field_04', 'numeric(9,3)' ],
            [ 'field_05', 'smallint' ],
            [ 'field_06', 'character varying(10)' ],
        );

        my @flds = map { $_->[0] } @fields_info;
        my $field_def = join " \n , ", map { join ' ', @{$_} } @fields_info;
        my $table_frn = 'test_info_frn';
        my $table     = 'test_info';

        my $ddl0 = qq{CREATE TABLE $table_frn (
                          field_10 CHAR(1)
                        , field_11 VARCHAR(10)
                        , CONSTRAINT pk_${table_frn}_field_10
                             PRIMARY KEY (field_10)
                     )
        };

        ok $engine->dbh->do($ddl0), "create '$table_frn' table";

        my $ddl = qq{CREATE TABLE $table ( \n   $field_def \n
                         , CONSTRAINT pk_${table}_field_00
                             PRIMARY KEY (field_00)
                         , CONSTRAINT fk__${table}_field_01
                             FOREIGN KEY (field_01)
                               REFERENCES $table_frn (field_10)
                                          ON DELETE NO ACTION
                                          ON UPDATE NO ACTION
                     )
        };

        ok $engine->dbh->do($ddl), "create '$table' table";

        ok $engine->table_exists($table), "$table table exists";

        cmp_deeply $engine->table_keys($table_frn), ['field_10'],
            'the pk keys data should match';

        cmp_deeply $engine->table_keys($table), ['field_00'],
            'the pk keys data should match';

        # cmp_deeply $engine->table_keys( $table, 'foreign' ),
        #     [ { key => 'field_01', references => $table_frn } ],
        #     'the fk keys data should match';
        cmp_deeply $engine->table_keys( $table, 'foreign' ),
            [ 'field_01' ],
            'the fk keys data should match';

        my $cols = $engine->get_columns($table);
        cmp_deeply $cols, \@flds, 'table columns';

        cmp_deeply $engine->table_list(), [$table_frn, $table], 'table list';

        ok my $info = $engine->get_info($table), 'get info for table';
        foreach my $rec (@fields_info) {
            my ($name, $type) = @{$rec};
            $type =~ s{\(.*\)}{}gmx;       # just the type
            $type =~ s{\s+precision}{}gmx; # just 'double', delete 'precision'
            $type =~ s{bigint}{int64}gmx;  # made with 'bigint' but is 'int64'
            is $info->{$name}{type}, $type, "type for field '$name' is '$type'";
        }


        ######################################################################
        # All done.
        done_testing;
    };
}

1;

use 5.010001;
use strict;
use warnings;
use Test::More;
use Cwd;
use Path::Tiny;
use Capture::Tiny 0.12 qw(capture_stdout);
use File::Path qw(remove_tree);

use App::Tpda3Dev::Config;
use App::Tpda3Dev::Info::Dist;
use App::Tpda3Dev::Command::Create;

ok my $conf = App::Tpda3Dev::Config->new, 'config constructor';

ok $conf->load, 'load test config files';

my $cwd  = Cwd::cwd();
my $path = path(qw{t output});

subtest 'the create command - SQLite' => sub {
    my $name = 'TestSq';

    ok my $info = App::Tpda3Dev::Info::Dist->new(
        dist_path => path( qw(t output) ),
    ), 'new dist info';
    ok my $configdir = $info->configdir;
    my $uri = "sqlite:$configdir/classicmodels.db";
    ok my $create = App::Tpda3Dev::Command::Create->new(
        config    => $conf,
        module    => $name,
        dist_path => $path,
        uri       => $uri,
    ), 'command constructor';

    like capture_stdout { $create->run }, qr/^\nCreating distribution/,
        'create command should work';

    cleanup($name);
};

subtest 'the create command - Pg' => sub {
    my $name = 'TestPg';

    ok my $info = App::Tpda3Dev::Info::Dist->new(
        dist_path => path( qw(t output) ),
    ), 'new dist info';
    ok my $configdir = $info->configdir;
    my $uri = 'db:pg://postgres:pass@localhost/classicmodels';
    ok my $create = App::Tpda3Dev::Command::Create->new(
        config    => $conf,
        module    => $name,
        dist_path => $path,
        uri       => $uri,
    ), 'command constructor';

    like capture_stdout { $create->run }, qr/^\nCreating distribution/,
        'create command should work';

    cleanup($name);
};

# Cleanup
sub cleanup {
    my $name = shift;
    chdir $path->stringify or die "Can't cd to 't/output': $!\n";
    remove_tree("Tpda3-$name") or warn $!;
    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
}

done_testing;

use Test2::V0;
use Path::Tiny;

use App::Tpda3Dev::Edit::Config;

my $cwd  = Cwd::cwd();

my $main_table = 'orders';
my $dep_table  = 'orderdetails';

my $app_path  = path( qw(t test-tree Tpda3-Test2App) );
my $conn_file = path( qw(share apps notdefa etc connection.yml) );

subtest 'Info with the default mnemonic name' => sub {
    chdir $app_path or die "Can't cd to '$app_path': $!\n";

    ok my $info = App::Tpda3Dev::Info->new(
        main_table => $main_table,
    ), 'new info instance';

    ok my $ec = App::Tpda3Dev::Edit::Config->new(
        name => 'scrtest',
        info => $info,
    ) , 'new config editor';

    like $ec->scrcfg_apfn->stringify, qr/scrtest.conf$/, 'screen config file name';

    todo 'fix test and module' => sub {
          ok $ec->config_update, 'update config';
    };
    
    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
};


done_testing;

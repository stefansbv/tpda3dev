use 5.010001;
use strict;
use warnings;
use utf8;
use Cwd;
use Test::More;
use Path::Tiny;

use App::Tpda3Dev::Render::Test;

my $cwd  = Cwd::cwd();
my $module = 'TestModule';

my $test = [
    [ 'test-load',       '00-load.t' ],
    [ 'test-config',     '10-config.t' ],
    [ 'test-connection', '20-connection.t' ],
];
foreach my $pair ( @{$test} ) {
    my $type = $pair->[0];
    my $temp = $pair->[1];
    ok my $ren = App::Tpda3Dev::Render::Test->new(
        module      => $module,
        mnemonic    => lc($module),
        templ_type  => $type,
        output_file => $temp,
        output_path => path( 't', 'output' ),
    ), 'new test';
    like $ren->get_template_for($type), qr/\.tt$/, "template for $type";
    ok $ren->render, "render test $temp";
}

# Cleanup
END {
    chdir path( qw(t output) )->stringify or die "Can't cd to 't/output': $!\n";
    foreach my $pair ( @{$test} ) {
        unlink $pair->[1] || warn $!;
    }
    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
}

done_testing;

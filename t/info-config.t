use 5.010001;
use utf8;
use Cwd;
use Path::Tiny;
use Test2::V0;

use App::Tpda3Dev::Info::Config;

## If this test is enabled, the next will fail because the default app
## config is already loaded.
# subtest 'The default Tpda3 app config' => sub {
#     ok my $dip = App::Tpda3Dev::Info::Config->new, 'New default instance';
#     is $dip->mnemonic, undef, 'mnemonic is undef';
#     ok $dip->cfname, 'has a cfname';
#     is $dip->config->application->{widgetset}, 'Tk', 'the widgetset is Tk';
#     ok $dip->apps_dir, 'the app_dir is defined';
#     ok $dip->module,   'has a module name';
# };

subtest 'The test-tk Tpda3 app config' => sub {
    ok my $dip = App::Tpda3Dev::Info::Config->new( mnemonic => 'test-tk' ),
        'New test-tk instance';
    is $dip->mnemonic, 'test-tk', 'mnemonic is test-tk';
    is $dip->tpda->application->{widgetset}, 'Tk', 'the widgetset is Tk';
    is path( $dip->apps_dir )->is_dir, 1, 'the app_dir exists';
    is $dip->module, 'Test', 'has a module name';
};

done_testing;

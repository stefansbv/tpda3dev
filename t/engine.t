#
# Borrowed and adapted from Sqitch v0.997 by @theory
#
use strict;
use warnings;
use 5.010;
use utf8;
use Test::Most;
use Path::Class;
use Locale::TextDomain qw(App-Tpda3Dev);
use App::Tpda3Dev;
use App::Tpda3Dev::Target;
use App::Tpda3Dev::X qw(hurl);
use lib 't/lib';

my $CLASS;

BEGIN {
    $CLASS = 'App::Tpda3Dev::Engine';
    use_ok $CLASS or die;
    # $ENV{TRANSFER_CONFIG} = 'nonexistent.conf';
}

can_ok $CLASS, qw(load new name uri);
my $die = '';
ENGINE: {
    # Stub out a engine.
    package App::Tpda3Dev::Engine::whu;
    use Moose;
    use App::Tpda3Dev::X qw(hurl);
    extends 'App::Tpda3Dev::Engine';
    $INC{'App/Tpda3Dev/Engine/whu.pm'} = __FILE__;

    my @SEEN;
    for my $meth (qw(
        get_info
    )) {
        no strict 'refs';
        *$meth = sub {
            hurl 'AAAH!' if $die eq $meth;
            push @SEEN => [ $meth => $_[1] ];
        };
    }

    sub seen { [@SEEN] }
    after seen => sub { @SEEN = () };
}

##############################################################################
# Test new().
ok my $target = App::Tpda3Dev::Target->new(
    uri      => 'db:firebird:',
), 'new target instance';

throws_ok { $CLASS->new }
    qr/\QAttribute (target) is required/,
    'Should get an exception for missing target param';
lives_ok { $CLASS->new( target => $target ) }
    'Should get no exception';

isa_ok $CLASS->new( { target => $target } ), $CLASS,
    'Engine';

##############################################################################
# Test load().
ok $target = App::Tpda3Dev::Target->new(
    uri      => 'db:whu:',
), 'new whu target';
ok my $engine = $CLASS->load({
    target   => $target,
}), 'Load a "whu" engine';
isa_ok $engine, 'App::Tpda3Dev::Engine::whu';

# Try an unknown engine.
$target = App::Tpda3Dev::Target->new(
    uri      => 'db:nonexistent:',
);
throws_ok { $CLASS->load( { target => $target } ) }
    'App::Tpda3Dev::X', 'Should get error for unsupported engine';
is $@->message, 'Unable to load App::Tpda3Dev::Engine::nonexistent',
    'Should get load error message';
like $@->previous_exception, qr/\QCan't locate/,
    'Should have relevant previoius exception';

# Test handling of an invalid engine.
throws_ok { $CLASS->load({ engine => 'nonexistent', target => $target }) }
    'App::Tpda3Dev::X', 'Should die on invalid engine';
is $@->message, __('Unable to load App::Tpda3Dev::Engine::nonexistent'),
    'Should get load error message';

TODO: {
    todo_skip 'On windows fails with: Can\'t call method \"previous_exception\" without a package or object reference at', 1;
    like $@->previous_exception, qr/\QCan't locate/,
        'Should have relevant previoius exception';
};

NOENGINE: {
    # Test handling of no target.
    throws_ok { $CLASS->load({}) } 'App::Tpda3Dev::X',
            'No target should die';
    is $@->message, 'Missing "target" parameter to load()',
        'It should be the expected message';
}

done_testing;

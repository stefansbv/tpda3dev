use 5.010001;
use utf8;
use Cwd;
use Path::Tiny;
use Test2::V0;

use App::Tpda3Dev::Info;

# The info as returned from the module

my $cwd  = Cwd::cwd();

my $main_table = 'orders';
my $dep_table  = 'orderdetails';

my $app1_path  = path( qw(t test-tree Tpda3-TestApp) );
my $app2_path  = path( qw(t test-tree Tpda3-Test2App) );
my $conn1_file = path( qw(share apps testapp etc connection.yml) );
my $conn2_file = path( qw(share apps notdefa etc connection.yml) );
my $conn_uri   = q(db:pg://localhost:5432/classicmodels);

subtest 'Info with the default mnemonic name' => sub {
    chdir $app1_path or die "Can't cd to '$app1_path': $!\n";

    ok my $info = App::Tpda3Dev::Info->new(
        main_table => $main_table,
    ), 'new info instance';

    is $info->dist->mnemonic, 'testapp', 'the default mnemonic name';

    is $info->conn_yaml_file, $conn1_file, 'conn_yaml_file';

    isa_ok $info->dist, ['App::Tpda3Dev::Info::Dist'], 'Info::Dist';

    isa_ok $info->db, ['App::Tpda3Dev::Info::Db'], 'Info::Db';

    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
};

subtest 'Info DB without main table' => sub {
    chdir $app2_path or die "Can't cd to '$app2_path': $!\n";

    ok my $info = App::Tpda3Dev::Info->new(
    ), 'new info instance';
    like(
        dies { $info->db->maintable_info },
        qr/The main table name should be provided/,
        'the main table name should be provided'
    );

    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
};

subtest 'Info with NOT the default mnemonic name' => sub {
    chdir $app2_path or die "Can't cd to '$app2_path': $!\n";

    ok my $info = App::Tpda3Dev::Info->new(
        main_table => $main_table,
    ), 'new info instance';

    is $info->dist->mnemonic, 'notdefa', 'the actual mnemonic name';

    is $info->conn_yaml_file, $conn2_file, 'conn_yaml_file';

    isa_ok $info->dist, ['App::Tpda3Dev::Info::Dist'], 'Info::Dist';

    isa_ok $info->db, ['App::Tpda3Dev::Info::Db'], 'Info::Db';

    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
};

END {
    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
}

done_testing;

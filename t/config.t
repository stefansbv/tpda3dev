#
# Test the Config module
#
use Test2::V0;

use Path::Tiny;
use File::HomeDir;

use App::Tpda3Dev::Config;

subtest 'Test with no config files' => sub {

    local $ENV{APP_T3D_SYS_CONFIG} = path(qw(t nonexistent.conf));
    local $ENV{APP_T3D_USR_CONFIG} = path(qw(t nonexistent.conf));

    ok my $conf = App::Tpda3Dev::Config->new, 'constructor';
};

subtest 'Test with the test config files' => sub {

    my $repo_path = path(qw(t test-repo));

    local $ENV{APP_T3D_SYS_CONFIG} = path(qw(t system.conf));
    local $ENV{APP_T3D_USR_CONFIG} = path(qw(t user.conf));

    ok my $conf = App::Tpda3Dev::Config->new, 'constructor';

    ok $conf->load, 'load test config files';
    is scalar @{ $conf->config_files }, 2, '2 config files loaded';
};

done_testing;

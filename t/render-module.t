use 5.010001;
use strict;
use warnings;
use utf8;
use Test::More;
use Test::Compile;
use Path::Tiny;

use App::Tpda3Dev::Render::Module;

ok my $data = {
    module      => 'TestModule',
    copy_author => 'Ștefan Suciu',
    copy_email  => 'me@example.com',
    copy_year   => '2016',
}, 'new data';

my $module = 'TestModule';
ok my $ren = App::Tpda3Dev::Render::Module->new(
    module      => $module,
    templ_data  => $data,
    templ_path  => path( 'share', 'templates' ),
    output_file => "$module.pm",
    output_path => path('t', 'output'),
), 'new module';

ok $ren->render, 'render main module file';

ok my $scr = path( 't', 'output', "$module.pm" )->stringify, 'the module file';
ok my $tc = Test::Compile->new, 'new test compile';
ok $tc->pl_file_compiles($scr), "$module.pm compiles ok";

# Cleanup
END {
    unlink $scr || warn $!;
}

done_testing;

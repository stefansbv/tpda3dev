use 5.010;
use strict;
use warnings;
use utf8;
use Test::More;
use Test::Exception;
use Test::Deep;
use Path::Tiny;

use App::Tpda3Dev::Edit::Menu;
use App::Tpda3Dev::Render::Menu;

my $expected_menu_data = [
    {   menu_user => {
            id    => 1001,
            label => 'User',
            popup => {
                1 => {
                    key       => undef,
                    label     => 'First menu label 1',
                    name      => 'FirstScreenName1',
                    sep       => 'none',
                    underline => 17,
                },
                2 => {
                    key       => undef,
                    label     => 'First menu label 2',
                    name      => 'FirstScreenName2',
                    sep       => 'none',
                    underline => 17,
                },
                3 => {
                    key       => undef,
                    label     => 'NewScreenName',
                    name      => 'NewScreenName',
                    sep       => 'none',
                    underline => 0,
                },

            },
            underline => 0,
        },
    },
    {   menu_dict => {
            id    => 1002,
            label => 'Second',
            popup => {
                1 => {
                    key       => undef,
                    label     => 'Second menu label 1',
                    name      => 'SecondScreenName1',
                    sep       => 'none',
                    underline => 19,
                },
            },
            underline => 0,
        },
    },
];

my $name      = 'NewScreenName';
my $menu_file = path 't', 'test-tree', 'menu.yml';

ok my $em = App::Tpda3Dev::Edit::Menu->new(
    name           => $name,
    menu_file_path => $menu_file,
), 'new editor';

ok my $menu_data = $em->menu_updated, 'the updated menu data';
cmp_deeply $menu_data, $expected_menu_data,
    'the menu data should match';

my $output_path = path( 't', 'output' );

ok my $ren = App::Tpda3Dev::Render::Menu->new(
    menudata    => $menu_data,
    output_path => $output_path,
), 'new render';
ok my $output_file = $ren->output_file, 'get the menu file';
is $output_file, 'menu.yml', 'the menu file is menu.yml';
like $ren->get_template_for('menu'), qr/\.tt$/, "template for menu";
ok $ren->render, "render test menu";

ok my $menu_path = path($output_path, $output_file)->absolute, 'the menu file';
like $menu_path, qr/menu\.yml/, 'the menu file looks fine';
say $menu_path;
lives_ok { Tpda3::Utils->read_yaml( $menu_path->stringify ) }
    'menu config load';

# Cleanup
END {
    unlink $menu_path || warn $!;
}

done_testing;

use 5.010001;
use Test2::V0;
use Cwd;
use Path::Tiny;
use Capture::Tiny 0.12 qw(capture_stdout);

use App::Tpda3Dev::Command::Info;

my $cwd  = Cwd::cwd();

subtest 'the info command' => sub {
    chdir $cwd;
    chdir path(qw{t test-tree Tpda3-TestApp})
        or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

    ok my $info = App::Tpda3Dev::Command::Info->new(
        action => 'dist',
    ), 'command constructor';

    like capture_stdout { $info->run }, qr/Current distribution/,
        'info dist command should work';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'the mnemo command' => sub {
    chdir $cwd;
    chdir path(qw{t test-tree Tpda3-TestApp})
        or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

    ok my $info = App::Tpda3Dev::Command::Info->new(
        action => 'mnemo',
    ), 'command constructor';

    like capture_stdout { $info->run }, qr/Configurations/,
        'info mnemo command should work';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'the table command' => sub {
    chdir $cwd;
    chdir path(qw{t test-tree Tpda3-TestApp})
        or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

    ok my $info = App::Tpda3Dev::Command::Info->new(
        action => 'table',
    ), 'command constructor';

    like capture_stdout { $info->run }, qr/Tables:/,
        'info table command should work';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'the table detail command' => sub {
    chdir $cwd;
    chdir path(qw{t test-tree Tpda3-TestApp})
        or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

    ok my $info = App::Tpda3Dev::Command::Info->new(
        action => 'table',
        detail => 'orders',
    ), 'command constructor';

    like capture_stdout { $info->run }, qr/(Columns|Keys) \[orders\]:/,
        'info table command should work';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'the screen command' => sub {
    chdir $cwd;
    chdir path(qw{t test-tree Tpda3-TestApp})
        or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

    ok my $info = App::Tpda3Dev::Command::Info->new(
        action => 'screen',
    ), 'command constructor';

    like capture_stdout { $info->run }, qr/Screen configurations:/,
        'info screen command should work';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

END {
    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
}

done_testing;

use 5.010001;
use strict;
use warnings;
use utf8;
use Test::More;
use Test::Compile;
use Path::Tiny;

use App::Tpda3Dev::Render::Screen::Module;

my $screen  = 'ScrTest';
my $config  = path( 't', 'test-tree', 'scrtest.conf' )->stringify;

my $ren = App::Tpda3Dev::Render::Screen::Module->new(
    module             => 'TestApp',
    screen             => $screen,
    screen_config_path => path($config),
    output_path        => path( 't', 'output' ),
    output_file        => "$screen.pm",
);

is $ren->screen_config_file, 'scrtest.conf', 'screen config file';
is $ren->get_template_for('screen'), 'screen.tt', 'template for screen';

ok $ren->render, 'render screen module file';

ok my $scr = path( 't', 'output', "$screen.pm" )->stringify, "the module: '$screen.pm'";
ok my $tc = Test::Compile->new, 'new test compile';
ok $tc->pl_file_compiles($scr), "$screen.pm compiles ok";

# Cleanup
END {
    unlink $scr || warn $!;
}

done_testing;

use 5.010001;
use utf8;
use Path::Tiny;
use Cwd;
use Test2::V0;

use App::Tpda3Dev::Info::Dist;

my $cwd = Cwd::cwd();

subtest 'New distribution context, good CWD' => sub {
    chdir path(qw{t test-tree new-app})
        or die "Can't cd to 't/test-tree/new-app': $!\n";

    # Assuming nonexistent 'NonExistent' app, and nonexistent
    # 'newtest' user configs for it in
    # '/home/user/.local/share/.tpda3/apps'
    ok my $info = App::Tpda3Dev::Info::Dist->new(
        module    => 'NonExistent',
    ), 'new instance';

    ok my $module = $info->module, 'the application name';
    is $module, 'NonExistent', 'the application name is ok';
    is $info->mnemonic, 'nonexistent', 'the mnemonic';
    is $info->dist_name, 'Tpda3-NonExistent',   'the dist name';
    ok my $module_path = $info->module_path, 'the modules rel path';;
    like $module_path, qr/App$/,                 'the modules rel path is ok';
    is $info->module_path_exists, undef,     'exists no modules rel path';
    like $info->screen_module_path, qr/${module}$/, 'the screen modules rel path is ok';
    like $info->relative_path_to_dist($module_path), qr/^Tpda3.+App$/,
        'the rel path to the main module';
    like (
        dies { $info->find_app_name },
        qr/Can't determine the distribution name/,
        'no main module name'
    );
    like $info->dist_sharedir, qr/apps$/, 'the dist share rel path';
    like $info->configdir, qr/\.?tpda3$/i,  'the app config abs path';
    like $info->dirtree, qr/dirtree$/,    'the app config abs path';
    like $info->testdir, qr/t$/,          'the test rel path';
    is $info->testdir_exists, undef,      'exists no test rel path';
    like (
        dies { $info->user_path_for },
        qr/No parameter/,

        'To few parameters for user_path_for'
    );
    like $info->user_path_for('etc'), qr/etc$/, 'the etc user config path';
    is $info->exists_user_path_for('etc'), undef, 'exists no etc user config path';
    like (
        dies { $info->dist_path_for },
        qr/No parameter/,
        'To few parameters for exists_user_path_for'
    );
    like $info->dist_path_for('etc'), qr/etc$/, 'the etc dist config path';
    is $info->exists_dist_path_for('etc'), undef, 'exists no etc dist config path';
    is $info->is_app_dir, undef, 'not a app dist dir';
    is $info->context, 'new', 'context';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'Existing distribution context, good CWD' => sub {
     chdir path(qw{t test-tree Tpda3-TestApp})
         or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

     # Assuming existent 'TestApp' app, and nonexistent 'testapp'
     # user configs for it in '/home/user/.local/share/.tpda3/apps'
     ok my $info = App::Tpda3Dev::Info::Dist->new(), 'new instance';

     is $info->module, undef, 'the application name, not yet';
     is $info->mnemonic, 'testapp', 'the mnemonic';
     ok my $module = $info->module, 'the application name';
     is $module, 'TestApp', 'the application name is ok';
     is $info->dist_name, 'Tpda3-TestApp',   'the dist name';
     is $info->find_app_name, 'TestApp', 'found the main module name';
     like $info->dist_sharedir, qr/apps$/, 'the dist share rel path';
     like $info->configdir, qr/\.?tpda3$/i,  'the app config abs path';
     like $info->dirtree, qr/dirtree$/,    'the app config abs path';
     like $info->testdir, qr/t$/,          'the test rel path';
     is $info->testdir_exists, 1,          'exists the test rel path';
     like (
         dies { $info->user_path_for },
         qr/No parameter/,
         'To few parameters for user_path_for'
     );
     like $info->user_path_for('etc'), qr/etc$/, 'the etc user config path';
     is $info->exists_user_path_for('etc'), undef, 'exists no etc user config path';
     like (
         dies { $info->dist_path_for },
         qr/No parameter/,
         'To few parameters for dist_path_for'
     );
     like $info->dist_path_for('etc'), qr/etc$/, 'the etc dist config path';
     is $info->exists_dist_path_for('etc'), 1, 'exists the etc dist config path';
     is $info->is_app_dir, 1, 'an app dist dir';
     is $info->context, 'update', 'context';
     is $info->find_mnemonic, 'testapp', 'find mnemonic';

     # TODO: add at least a screen module and a non screen module for test
     # is $info->screen_module_list, [], 'find screens';

     chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'New distribution context, wrong CWD' => sub {
    chdir path(qw{t test-tree Tpda3-TestApp})
        or die "Can't cd to 't/test-tree/Tpda3-TestApp': $!\n";

    # Assuming nonexistent 'NonExistent' app, and nonexistent
    # 'newtest' user configs for it in
    # '/home/user/.local/share/.tpda3/apps'
    ok my $info = App::Tpda3Dev::Info::Dist->new(
        module    => 'NonExistent',
    ), 'new instance';

    ok my $module = $info->module, 'the application name';
    is $module, 'NonExistent', 'the application name is ok';
    is $info->mnemonic, 'nonexistent', 'the mnemonic';
    is $info->dist_name, 'Tpda3-NonExistent',   'the dist name';
    ok my $module_path = $info->module_path, 'the modules rel path';;
    like $module_path, qr/App$/,                 'the modules rel path is ok';
    is $info->module_path_exists, 1,         'exists the modules rel path';
    like $info->screen_module_path, qr/${module}$/, 'the screen modules rel path is ok';
    like $info->relative_path_to_dist($module_path), qr/^Tpda3.+App$/,
        'the rel path to the main module';
    is $info->find_app_name, 'TestApp', 'found the main module name';
    like $info->dist_sharedir, qr/apps$/, 'the dist share rel path';
    like $info->configdir, qr/\.?tpda3$/i,  'the app config abs path';
    like $info->dirtree, qr/dirtree$/,    'the app config abs path';
    like $info->testdir, qr/t$/,          'the test rel path';
    is $info->testdir_exists, 1,          'exists the test rel path';
    like (
        dies { $info->user_path_for },
        qr/No parameter/,
        'To few parameters for user_path_for'
    );
    like $info->user_path_for('etc'), qr/etc$/, 'the etc user config path';
    is $info->exists_user_path_for('etc'), undef, 'exists no etc user config path';
    like (
        dies { $info->dist_path_for },
        qr/No parameter/,
        'To few parameters for exists_user_path_for'
    );
    like $info->dist_path_for('etc'), qr/etc$/, 'the etc dist config path';
    is $info->exists_dist_path_for('etc'), undef, 'exists no etc dist config path';
    is $info->is_app_dir, undef, 'not a app dist dir';
    is $info->context, 'new', 'context';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

subtest 'Existing distribution context, wrong CWD' => sub {
    chdir path(qw{t test-tree new-app})
        or die "Can't cd to 't/test-tree/new-app': $!\n";

    # Assuming existent 'TestApp' app, and nonexistent 'testapp'
    # user configs for it in '/home/user/.local/share/.tpda3/apps'
    ok my $info = App::Tpda3Dev::Info::Dist->new, 'new instance';

    is $info->module, undef, 'the application name, not yet';
    like (
        dies { $info->mnemonic },
        qr/Can't determine the distribution name/,
        'no main module name'
    );
    is $info->module, undef, 'no application name';
    like (
        dies { $info->dist_name },
        qr/No module name/,
        'no dist name'
    );
    like (
        dies { $info->find_app_name },
        qr/Can't determine the distribution name/,
        'no main module name'
    );
    like $info->dist_sharedir, qr/apps$/, 'the dist share rel path';
    like $info->configdir, qr/\.?tpda3$/i,  'the app config abs path';
    like $info->dirtree, qr/dirtree$/,    'the app config abs path';
    like $info->testdir, qr/t$/,          'the test rel path';
    is $info->testdir_exists, undef,      'no test rel path';
    # like (
    #   dies { $info->user_path_for },
    #   qr/No parameter/,
    #   'To few parameters for user_path_for'
    # );
    like (
        dies { $info->user_path_for('etc') },
        qr/Can't determine the distribution name/,
        'no etc user config path'
    );
    like (
        dies { $info->exists_user_path_for('etc') },
        qr/Can't determine the distribution name/,
        'exists no etc user config path'
    );
    # like (
    #   dies { $info->dist_path_for },
    #   qr/No parameter/,
    #     'To few parameters for dist_path_for'
    # );
    like (
        dies { $info->dist_path_for('etc') },
        qr/Can't determine the distribution name/,
        'no etc dist config path'
    );
    like (
        dies { $info->exists_dist_path_for('etc') },
        qr/Can't determine the distribution name/,
        'exists no etc dist config path'
    );
    is $info->is_app_dir, undef, 'not a app dist dir';
    is $info->context, 'new', 'context';
    is $info->find_mnemonic, undef, 'find mnemonic';

    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
};

END {
    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
}

done_testing;

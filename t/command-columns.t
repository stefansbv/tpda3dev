use 5.010001;
use Test2::V0;
use Cwd;
use Path::Tiny;
use Capture::Tiny 0.12 qw(capture_stdout);
use File::Path qw(remove_tree);

use App::Tpda3Dev::Config;
use App::Tpda3Dev::Info;
use App::Tpda3Dev::Command::Columns;

my $cwd  = Cwd::cwd();
my $info;

my $main_table = 'orders';
my $dep_table  = 'orderdetails';

my $app_path  = path( qw(t test-tree Tpda3-TestApp) );
my $conn_file = path( qw(share apps testapp etc connection.yml) );

chdir $app_path or die "Can't cd to '$app_path': $!\n";

subtest 'the columns new command' => sub {
    ok my $columns = App::Tpda3Dev::Command::Columns->new(
        action => 'new',
        table  => 'employees',
    ), 'command constructor';
    ok $info = $columns->info, 'store the info';
    # like capture_stdout { $columns->run }, qr/^\nGenerating columns/,
    #     'columns command should work';
};

# Cleanup
END {
    # unlink path $info->dist->dist_path_for('scr'), 'columns.conf';
    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
}

done_testing;

use 5.010001;
use utf8;
use Path::Tiny;
use Test2::V0;

use App::Tpda3Dev::Info::Db::Connection;

my $conn_file = path( qw(t test-tree connection.yml) );
my $conn_uri  = q(db:pg://localhost:5432/classicmodels);

subtest 'Connection config from yaml file' => sub {
    ok my $db = App::Tpda3Dev::Info::Db::Connection->new(
        conn_yaml_file => $conn_file,
    ), 'new instance';
    like $db->uri_db, qr/^db:pg/, 'the uri built from a connection file';
    is $db->driver, 'pg', 'the engine';
    is $db->host, 'localhost', 'the host';
    is $db->port, '5432', 'the port';
    is $db->dbname, 'classicmodels', 'the dbname';
    is $db->user, undef, 'the user name';
    is $db->role, undef, 'the role name';
    like  $db->uri, qr/classicmodels$/, 'the uri';
};

subtest 'Connection config from URI string' => sub {
    ok my $db = App::Tpda3Dev::Info::Db::Connection->new(
        uri => $conn_uri,
    ), 'new instance';
    like $db->uri_db, qr/^db:pg/, 'the uri built from a connection file';
    is $db->driver, 'pg', 'the engine';
    is $db->host, 'localhost', 'the host';
    is $db->port, '5432', 'the port';
    is $db->dbname, 'classicmodels', 'the dbname';
    is $db->user, undef, 'the user name';
    is $db->role, undef, 'the role name';
    like  $db->uri, qr/classicmodels$/, 'the uri';
};

subtest 'Connection config from void' => sub {
    ok my $db = App::Tpda3Dev::Info::Db::Connection->new, 'new instance';
	like (
		dies { $db->uri_db },
		qr/A connection file or an URI/,
		'a connection file or an URI is expected'
	);
};

done_testing;

use 5.010001;
use utf8;
use Cwd;
use Path::Tiny;
use Test2::V0;

use App::Tpda3Dev::Utils;

my $cwd = Cwd::cwd();

ok my $util = App::Tpda3Dev::Utils->new, 'new utils instance';

my $app_path = path( qw(t test-tree Tpda3-TestApp share apps testapp) );
my $out_path = path( qw(t output utils-test) );

my $app_path_scr = path $app_path, 'scr';
my $app_path_tex = path $app_path, 'tex';

subtest 'Files from the scr subdir' => sub {
    my $expected = [qw(customers.conf orders.conf products.conf)];
    ok my $filelist = $util->file_list($app_path_scr), 'get the scr file list';
    my @namelist =
        sort map { path($_)->relative($app_path_scr)->stringify } @{$filelist};
    is \@namelist, $expected, 'file list is as expected';
};

subtest 'Files from the tex subdir' => sub {
    my $expected =
      [qw(model output model/keepme model/template-ro.tt output/keepme)];
    ok my $filelist = $util->file_list($app_path_tex), 'get the tex file list';
    my @namelist =
      map { path($_)->relative($app_path_tex)->stringify } @{$filelist};
    is \@namelist, $expected, 'file list is as expected';
};

subtest 'Compare files' => sub {
    my $src = path $app_path_scr, 'orders.conf';
    my $dst = path $out_path, 'orders.conf';
    todo "Skip until fixed on windows" => sub {
        ok $util->is_selfsame($src, $dst), 'is selfsame';
    };
    ok !$util->is_selfsame(path(qw{t user.conf}), $dst), 'not selfsame';
    todo "Skip until fixed on windows" => sub {
        is $util->get_perms($src), '0644', 'get file perms';
    };
};

subtest 'Path tests' => sub {
    ok( lives { $util->make_path(path $out_path, 'done-path') },
        'should have no error for make_path' );
    my $src = path $app_path_scr, 'customers.conf';
    my $dst = path $out_path, 'customers.conf';
    ok( lives { $util->copy_file_local($src, $dst) },
        'should have no error for copy_file_local' );
    unlink $dst;
};

END {
    rmdir path $out_path, 'done-path';
}

done_testing;

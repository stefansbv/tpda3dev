use 5.010001;
use strict;
use warnings;
use utf8;
use Cwd;
use Test::More;
use Test::Exception;
use Path::Tiny;

use App::Tpda3Dev::Render::Menu;

my $output_file = 'menu.yml';
my $output_path = path( 't', 'output' );

my $menu_data = [
    {   menu_user => {
            id    => 1001,
            label => "User",
            popup => {
                1 => {
                    key       => undef,
                    label     => "First menu label 1",
                    name      => "FirstScreenName1",
                    sep       => "none",
                    underline => 17,
                },
                2 => {
                    key       => undef,
                    label     => "First menu label 2",
                    name      => "FirstScreenName2",
                    sep       => "none",
                    underline => 17,
                },
            },
            underline => 0,
        },
    },
    {   menu_dict => {
            id    => 1002,
            label => "Second",
            popup => {
                1 => {
                    key       => undef,
                    label     => "Second menu label 1",
                    name      => "SecondScreenName1",
                    sep       => "none",
                    underline => 19,
                },
            },
            underline => 0,
        },
    },
];

ok my $ren = App::Tpda3Dev::Render::Menu->new(
    menudata    => $menu_data,
    output_file => $output_file,
    output_path => $output_path,
    ), 'new test';
like $ren->get_template_for('menu'), qr/\.tt$/, "template for menu";
ok $ren->render, "render test menu";

my $menu_conf = path $output_path, $output_file;
lives_ok { Tpda3::Utils->read_yaml( $menu_conf->stringify ) }
    'menu config load';

# Cleanup
END {
    unlink $menu_conf || warn $!;
}

done_testing;

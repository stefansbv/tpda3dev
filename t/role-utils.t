use 5.010;
use strict;
use warnings;

use Test::More;

use lib 't/lib';
use RoleUtilsTest;

my $t = RoleUtilsTest->new;

my $tests = [
    [ 'numeric',               { type_name => 'numeric' } ],
    [ 'numeric(9,3)',          { type_name => 'numeric', scale => 3, precision => 9 } ],
    [ 'varchar(10)',           { type_name => 'varchar', length => 10 } ],
    [ 'varchar',               { type_name => 'varchar' } ],
    [ 'character varying(10)', { type_name => 'character varying', length => 10 } ],
];

foreach my $type (@$tests) {
    ok my $rec = $t->parse_typenames( $type->[0] ), "test with '$type->[0]'" ;
    is_deeply $rec, $type->[1], 'record match';
}

done_testing;

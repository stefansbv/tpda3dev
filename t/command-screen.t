use 5.010001;
use Test2::V0;
use Cwd;
use Path::Tiny;
use Capture::Tiny 0.12 qw(capture_stdout);
use File::Path qw(remove_tree);

use App::Tpda3Dev::Config;
use App::Tpda3Dev::Info;
use App::Tpda3Dev::Command::Screen;

my $cwd  = Cwd::cwd();
my $info;

my $app_path  = path( qw(t test-tree Tpda3-TestApp) );
my $conn_file = path( qw(share apps testapp etc connection.yml) );

chdir $app_path or die "Can't cd to '$app_path': $!\n";

subtest 'the screen new command' => sub {
    ok my $screen = App::Tpda3Dev::Command::Screen->new(
        action     => 'new',
        screen     => 'Orders2',
        main_table => 'orders',
        table      => 'orderdetails',
    ), 'command constructor';
    ok $info = $screen->info, 'store the info';
    like capture_stdout { $screen->run }, qr/^\nCreating screen/,
        'screen command should work';
};

subtest 'the screen new command' => sub {
    ok my $screen = App::Tpda3Dev::Command::Screen->new(
        action     => 'new',
        screen     => 'Employees',
        main_table => 'employees',
    ), 'command constructor';
    ok $info = $screen->info, 'store the info';
    like capture_stdout { $screen->run }, qr/^\nCreating screen/,
        'screen command should work';
};

# Cleanup
END {
    unlink path $info->dist->dist_path_for('scr'), 'orders2.conf';
    unlink path $info->dist->screen_module_path, 'Orders2.pm';
    unlink path $info->dist->dist_path_for('scr'), 'employees.conf';
    unlink path $info->dist->screen_module_path, 'Employees.pm';

    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
}

done_testing;

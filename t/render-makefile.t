use 5.010001;
use strict;
use warnings;
use utf8;
use Test::Compile;
use Test::More;
use Path::Tiny;

use App::Tpda3Dev::Render::Makefile;

ok my $data = {
    module      => 'TestModule',
    copy_author => 'Ștefan Suciu',
    copy_email  => 'someone@exampl.com',
    copy_year   => '2016',
}, 'the data';

ok my $ren = App::Tpda3Dev::Render::Makefile->new(
    module      => 'TestModule',
    templ_path  => path( 'share', 'templates' ),
    templ_data  => $data,
    output_path => path('t', 'output'),
), 'new makefile';

is $ren->get_template_for('makefile'), 'makefile.tt', 'template for makefile';

ok $ren->render, 'render makefile file';

ok my $scr = path( 't', 'output', 'Makefile.PL' )->stringify, 'the makefile';
SKIP: {
    skip "Can't call method 'load_all_extensions' on an undefined value", 2;
    ok my $tc  = Test::Compile->new, 'new test compile';
    ok $tc->pl_file_compiles($scr);
}

# Cleanup
END {
    unlink $scr || warn $!;
}

done_testing;

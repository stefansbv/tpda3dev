use 5.010001;
use utf8;
use Cwd;
use Path::Tiny;
use File::HomeDir;
use Test2::V0;
use Test2::Tools::Subtest qw/subtest_streamed/;

use App::Tpda3Dev::X qw(hurl);
use App::Tpda3Dev::Info::Db;

my $cwd  = Cwd::cwd();

my $main_table = 'orders';
my $dep_table  = 'orderdetails';

my $app_path  = path( qw{t test-tree Tpda3-TestApp} );
my $conn_file = path( qw{share apps testapp etc connection.yml} );

my $dbfile    = get_testdb_filename();
my $conn_uri  = qq(dbi:SQLite:dbname=$dbfile", '', '');

chdir $app_path or die "Can't cd to '$app_path': $!\n";

subtest_streamed 'Info DB without parameters' => sub {
        ok my $db = App::Tpda3Dev::Info::Db->new;
    isa_ok $db->info_conn, ['App::Tpda3Dev::Info::Db::Connection'],
        'info conection';
    like(
        dies { $db->target },
        qr/\QA connection file or an URI is required/,
        'Should get an exception for missing params'
    );
};

subtest_streamed 'Info DB with URI' => sub {
    ok my $db = App::Tpda3Dev::Info::Db->new(
        uri => $conn_uri,
    ), 'new db instance';
    is $db->conn_yaml_file, undef,
        'the connection file attribute shoud not be set';
    isa_ok $db, ['App::Tpda3Dev::Info::Db'],
        'Info::Db';
    isa_ok $db->info_conn, ['App::Tpda3Dev::Info::Db::Connection'],
        'Info::Db::Connection';
};

subtest_streamed 'Info DB without main table' => sub {
    ok my $db = App::Tpda3Dev::Info::Db->new( conn_yaml_file => $conn_file, ),
        'new db instance';
    like(
        dies { $db->maintable_info },
        qr/The main table name should be provided/,
        'the main table name should be provided'
    );
};

subtest_streamed 'Info DB with main table' => sub {
    ok my $db = App::Tpda3Dev::Info::Db->new(
        conn_yaml_file => $conn_file,
        main_table     => $main_table,
    ), 'new db instance';
    like $db->conn_yaml_file->stringify, qr/connection.yml$/,
        'the connection file attribute shoud be set';
    isa_ok $db, ['App::Tpda3Dev::Info::Db'],
        'Info::Db';
    isa_ok $db->info_conn, ['App::Tpda3Dev::Info::Db::Connection'],
        'Info::Db::Connection';

    my $collist = [
        "ordernumber", "orderdate", "requireddate",   "shippeddate",
        "statuscode",  "comments",  "customernumber", "ordertotal",
    ];

    ok my $maininfo = $db->maintable_info, 'the main table info data';

    is $maininfo->{name}, $main_table, 'maintable info: name';
    is $maininfo->{pk_keys}, ['ordernumber'], 'maintable info: pk_keys';
    # is $maininfo->{fk_keys}, [ { key => 'customernumber', references
    # => 'customers' } ], 'maintable info: fk_keys';
    is $maininfo->{fk_keys}, [ 'customernumber' ], 'maintable info: fk_keys';
    is $maininfo->{collist}, $collist, 'maintable info: collist';
    my $colinfo = {
        defa        => "0.00",
        is_nullable => 1,
        length      => undef,
        name        => "ordertotal",
        pos         => 7,
        prec        => 10,
        scale       => 2,
        type        => "decimal",
    };

    is $maininfo->{colinfo}{ordertotal}, $colinfo, 'maintable info: colinfo';
    ok my $maindata = $db->maintable_data, 'the main table data';
    is $maindata->{collist}, $collist, 'maintable data collist';
    # Columns
    is $maindata->{keyfields}, ["ordernumber"], 'maintable data keyfields';
    is $maindata->{name}, "orders", 'maintable data name';
    is $maindata->{view}, "orders", 'maintable data view';

    is( $maindata->{columns}[0],
        hash {
            field bgcolor     => "white";
            field ctrltype    => match qr/\w/;
            field datatype    => match qr/integer|numeric|alphanumplus/;
            field displ_width => match qr/\d+/;
            field findtype    => match qr/\w+/;
            field label       => match qr/[\w\s]+/;
            field name        => match qr/\w+/;
            field numscale    => 0;
            field readwrite   => match qr/ro|rw/;
            field state       => match qr/enable|disabled/;
            field valid_width => match qr/\d+/;
            end;    # Specify that there should be no more elements.
        },
        'the main table data should match'
    );
};

subtest_streamed 'Info DB with main table & dep table' => sub {
    ok my $db = App::Tpda3Dev::Info::Db->new(
        conn_yaml_file => $conn_file,
        main_table     => $main_table,
        dep_tables     => [$dep_table],
    ), 'new db instance';
    isa_ok $db, ['App::Tpda3Dev::Info::Db'],
        'Info::Db';
    isa_ok $db->info_conn, ['App::Tpda3Dev::Info::Db::Connection'],
        'Info::Db::Connection';

    ok my $depinfo = $db->deptables_info, 'the dep table info data';
    my $collist = [
        "ordernumber", "orderlinenumber",
        "productcode", "quantityordered",
        "priceeach",
    ];
    is $depinfo->[0]{pk_keys}, [ "ordernumber", "orderlinenumber" ],
        'deptable info: pk_keys';
    is(
        $depinfo->[0]{fk_keys},
        array {
        item match qr/ordernumber|productcode/;
        item match qr/productcode|ordernumber/;
        end;
    },
    "array matches expectations"
        # hash {
        #     field key => match qr/ordernumber|productcode/;
        #     field references => match qr/products|orders/;
        #     end;
        # },
        # "hash matches expectations"
    );

    is $depinfo->[0]{collist}, $collist, 'maintable info: collist';
    my $colinfo = {
        defa        => undef,
        is_nullable => 0,
        length      => undef,
        name        => "orderlinenumber",
        pos         => 1,
        prec        => undef,
        scale       => undef,
        type        => "smallint",
    };
    is $depinfo->[0]{colinfo}{orderlinenumber}, $colinfo,
        'deptable info: colinfo';

    # Dependent tables data
    ok my $depdata = $db->deptables_data, 'the dep table data';
    is $depdata->[0]{collist}, [
        "ordernumber", "orderlinenumber", "productcode", "quantityordered",
        "priceeach" ], 'collist should match';
    is $depdata->[0]{keyfields},
        [ "ordernumber", "orderlinenumber" ], 'keyfields should match';
    is $depdata->[0]{name}, "orderdetails", 'table name should match';
    is $depdata->[0]{view}, "orderdetails", 'view name should match';
    is $depdata->[0]{orderby}, [ 'ordernumber', 'orderlinenumber' ],
        'orderby should match';

    # Columns, testing only the keys from one record
    is( $depdata->[0]{columns}[0],
        hash {
            field datatype    => match qr/integer|numeric|alphanumplus/;
            field displ_width => match qr/\d+/;
            field id          => match qr/\d/;
            field label       => match qr/\w+/;
            field name        => match qr/\w+/;
            field numscale    => match qr/\d/;
            field readwrite   => "rw";
            field tag         => "ro_center";
            field valid_width => match qr/\d+/;
            end;    # Specify that there should be no more elements.
        },
        'the dep table info data should match'
    );
};

END {
    chdir $cwd or die "Can't cd back to '$cwd': $!\n";
}

sub get_testdb_filename {
    return path(File::HomeDir->my_data, 'classicmodels.db');
}

done_testing;

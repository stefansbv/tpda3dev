use 5.010001;
use strict;
use warnings;
use utf8;
use Test::More;
use Path::Tiny;

use App::Tpda3Dev::Render::Readme;

ok my $data = {
    module      => 'TestModule',
    copy_author => 'Ștefan Suciu',
    copy_email  => 'me@example.com',
    copy_year   => '2016',
}, 'the data';

ok my $ren = App::Tpda3Dev::Render::Readme->new(
    module      => 'TestModule',
    templ_data  => $data,
    output_path => path('t', 'output'),
), 'new readme';

is $ren->get_template_for('readme'), 'readme.tt', 'template for readme';

ok $ren->render, 'render readme file';

done_testing;

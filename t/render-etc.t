use 5.010;
use strict;
use warnings;
use utf8;
use Cwd;
use Test::More;
use Path::Tiny;
use File::Path qw(remove_tree);

use App::Tpda3Dev::Render::YAML;
use App::Tpda3Dev::Info::Dist;

my $cwd  = Cwd::cwd();
my $module = 'TestModule';
ok my $info = App::Tpda3Dev::Info::Dist->new(
    module    => $module,
    dist_path => path qw(t output),
), 'new instance';
ok my $configdir = $info->configdir, 'the config dir';
ok my $uri = "sqlite:$configdir/classicmodels.db", 'the uri';
ok my $etcdir = $info->relative_path_to_dist( $info->dist_path_for('etc') ),
    'the etc dir';

my $test_data = {
    'cfg-application' => 'application.yml',
    'cfg-menu'        => 'menu.yml',
    'cfg-connection'  => 'connection.yml',
};

# Make etc/*.yml configs
my $conf = [
    [ 'cfg-application', 'application.yml' ],
    [ 'cfg-menu',        'menu.yml' ],
    [ 'cfg-connection',  'connection.yml' ],
];
foreach my $pair ( @{$conf} ) {
    my $type = $pair->[0];
    my $temp = $pair->[1];
    ok my $ren = App::Tpda3Dev::Render::YAML->new(
        module      => $module,
        mnemonic    => lc $module,
        uri         => $uri,
        templ_type  => $type,
        output_file => $temp,
        output_path => $etcdir,
    ), 'new yaml';;
    like $ren->get_template_for($type), qr/\.tt$/, "template for $type";
    ok $ren->render, "render file $temp";
}

# Cleanup
END {
    chdir path( qw(t output) )->stringify or die "Can't cd to 't/output': $!\n";
    remove_tree("Tpda3-$module") or warn $!;
    chdir $cwd or warn "Can't cd back to '$cwd': $!\n";
}

done_testing;

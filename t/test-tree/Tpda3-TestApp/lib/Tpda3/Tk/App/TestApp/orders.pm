package Tpda3::Tk::App::TestApp::orders;

use strict;
use warnings;
use utf8;

use Tk::widgets qw(DateEntry JComboBox);

use base q{Tpda3::Tk::Screen};

use POSIX qw (strftime);

use Tpda3::Utils;

=head1 NAME

Tpda3::Tk::App::TestApp::orders screen.

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

    require Tpda3::App::TestApp::orders;

    my $scr = Tpda3::App::TestApp::orders->new;

    $scr->run_screen($args);

=head1 METHODS

=head2 run_screen

The screen layout

=cut

sub run_screen {
    my ( $self, $nb ) = @_;

    my $rec_page  = $nb->page_widget('rec');
    my $det_page  = $nb->page_widget('det');
    $self->{view} = $nb->toplevel;
    $self->{bg}   = $self->{view}->cget('-background');

    my $validation
        = Tpda3::Tk::Validation->new( $self->{scrcfg}, $self->{view} );

    my $date_format = $self->{scrcfg}->app_dateformat();

    #- For DateEntry day names
    my $daynames = [qw(D L Ma Mi J V S)];

    #- Frame - top

    my $frm_top = $rec_page->LabFrame(
        -foreground => 'blue',
        -label      => 'orders',
        -labelside  => 'acrosstop',
    );
    $frm_top->grid(
        $frm_top,
        -row    => 0, -column => 0,
        -ipadx  => 3, -ipady  => 3,
        -sticky => 'nsew',
    );

    my $f1d = 110;              # distance from left


    #- Ordernumber (ordernumber)

    my $lordernumber = $frm_top->Label( -text => 'Ordernumber' );
    $lordernumber->form(
        -top     => [ %0, 0 ],
        -left    => [ %0, 0 ],
        -padleft => 5,
    );

    my $eordernumber = $frm_top->MEntry(
        -width              => 10,
        -disabledbackground => $self->{bg},
        -disabledforeground => 'black',
    );
    $eordernumber->form(
        -top  => [ '&', $lordernumber, 0 ],
        -left => [ %0,  $f1d ],
    );

    # Font
    my $my_font = $eordernumber->cget('-font');


    #- Orderdate (orderdate)

    my $vorderdate;
    my $lorderdate = $frm_top->Label( -text => 'Orderdate' );
    $lorderdate->form(
        -top  => [ $lordernumber, 8 ],
        -left => [ %0, 0 ],
        -padleft => 5,
    );

    my $dorderdate = $frm_top->DateEntry(
        -daynames        => $daynames,
        -variable        => \$vorderdate,
        -arrowimage      => 'calmonth16',
        -todaybackground => 'lightblue',
        -weekstart       => 1,
        -parsecmd        => sub {
            Tpda3::Utils->dateentry_parse_date( $date_format, @_ );
        },
        -formatcmd => sub {
            Tpda3::Utils->dateentry_format_date( $date_format, @_ );
        },
    );
    $dorderdate->form(
        -top  => [ '&', $lorderdate, 0 ],
        -left => [ %0, $f1d ],
    );


    #- Requireddate (requireddate)

    my $vrequireddate;
    my $lrequireddate = $frm_top->Label( -text => 'Requireddate' );
    $lrequireddate->form(
        -top  => [ $lorderdate, 8 ],
        -left => [ %0, 0 ],
        -padleft => 5,
    );

    my $drequireddate = $frm_top->DateEntry(
        -daynames        => $daynames,
        -variable        => \$vrequireddate,
        -arrowimage      => 'calmonth16',
        -todaybackground => 'lightblue',
        -weekstart       => 1,
        -parsecmd        => sub {
            Tpda3::Utils->dateentry_parse_date( $date_format, @_ );
        },
        -formatcmd => sub {
            Tpda3::Utils->dateentry_format_date( $date_format, @_ );
        },
    );
    $drequireddate->form(
        -top  => [ '&', $lrequireddate, 0 ],
        -left => [ %0, $f1d ],
    );


    #- Shippeddate (shippeddate)

    my $vshippeddate;
    my $lshippeddate = $frm_top->Label( -text => 'Shippeddate' );
    $lshippeddate->form(
        -top  => [ $lrequireddate, 8 ],
        -left => [ %0, 0 ],
        -padleft => 5,
    );

    my $dshippeddate = $frm_top->DateEntry(
        -daynames        => $daynames,
        -variable        => \$vshippeddate,
        -arrowimage      => 'calmonth16',
        -todaybackground => 'lightblue',
        -weekstart       => 1,
        -parsecmd        => sub {
            Tpda3::Utils->dateentry_parse_date( $date_format, @_ );
        },
        -formatcmd => sub {
            Tpda3::Utils->dateentry_format_date( $date_format, @_ );
        },
    );
    $dshippeddate->form(
        -top  => [ '&', $lshippeddate, 0 ],
        -left => [ %0, $f1d ],
    );


    #- Statuscode (statuscode)

    my $lstatuscode = $frm_top->Label( -text => 'Statuscode' );
    $lstatuscode->form(
        -top     => [ $lshippeddate, 8 ],
        -left    => [ %0, 0 ],
        -padleft => 5,
    );

    my $estatuscode = $frm_top->MEntry(
        -width              => 10,
        -disabledbackground => $self->{bg},
        -disabledforeground => 'black',
    );
    $estatuscode->form(
        -top  => [ '&', $lstatuscode, 0 ],
        -left => [ %0,  $f1d ],
    );


    #- Comments (comments)

    my $lcomments = $frm_top->Label(
        -text => 'Comments',
    );
    $lcomments->form(
        -top     => [ $lstatuscode, 8 ],
        -left    => [ %0, 0 ],
        -padleft => 5,
    );

    my $tcomments = $frm_top->Scrolled(
        'Text',
        -width      => 30,
        -height     => 3,
        -wrap       => 'word',
        -scrollbars => 'e',
        -font       => $my_font,
    );
    $tcomments->form(
        -top  => [ '&', $lcomments, 0 ],
        -left => [ %0,  $f1d ],
    );


    #- Customernumber (customernumber)

    my $lcustomernumber = $frm_top->Label( -text => 'Customernumber' );
    $lcustomernumber->form(
        -top     => [ $lcomments, 43 ],
        -left    => [ %0, 0 ],
        -padleft => 5,
    );

    my $ecustomernumber = $frm_top->MEntry(
        -width              => 10,
        -disabledbackground => $self->{bg},
        -disabledforeground => 'black',
    );
    $ecustomernumber->form(
        -top  => [ '&', $lcustomernumber, 0 ],
        -left => [ %0,  $f1d ],
    );


    #- Ordertotal (ordertotal)

    my $lordertotal = $frm_top->Label( -text => 'Ordertotal' );
    $lordertotal->form(
        -top     => [ $lcustomernumber, 8 ],
        -left    => [ %0, 0 ],
        -padleft => 5,
    );

    my $eordertotal = $frm_top->MEntry(
        -width              => 10,
        -justify            => 'right',
        -disabledbackground => $self->{bg},
        -disabledforeground => 'black',
    );
    $eordertotal->form(
        -top  => [ '&', $lordertotal, 0 ],
        -left => [ %0,  $f1d ],
    );



    # Entry objects: var_asoc, var_obiect.
    # The configurations are defined in 'orders.conf'.
    $self->{controls} = {
        ordernumber => [ undef, $eordernumber ],
        orderdate => [ \$vorderdate, $dorderdate ],
        requireddate => [ \$vrequireddate, $drequireddate ],
        shippeddate => [ \$vshippeddate, $dshippeddate ],
        statuscode => [ undef, $estatuscode ],
        comments => [ undef, $tcomments ],
        customernumber => [ undef, $ecustomernumber ],
        ordertotal => [ undef, $eordertotal ],
    };

    return;
}


=head1 AUTHOR

, C<< <> >>

=head1 BUGS

None known.

Please report any bugs or feature requests to the author.

=head1 LICENSE AND COPYRIGHT

Copyright 2020 

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation.

=cut

1; # End of Tpda3::App::TestApp::orders


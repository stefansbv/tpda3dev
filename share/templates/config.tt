# Screen settings
# Style: default or report
<screen>
  version               = 5
  name                  = [% screen %]
  description           = [% screen %] !description here!
  style                 = default
  geometry              = 520x520+20+20
  <details/>
</screen>

# Report file name bound to the print toolbar button of the Screen
<defaultreport/>

# Document template file name bound to the edit toolbar button of the
# Screen
<defaultdocument/>

# Data source for list widgets (JCombobox)
<lists_ds/>

# Column names in the 'List' tab table, chosen from the field names
# from the main table view. The order is preserved.
<list_header>
[%- IF keyfields.size == 1 -%]
 [% FOR col IN keyfields %]
  lookup                = [ [%- col -%] ]
 [%- END -%]
[%- ELSE -%]
 [% FOR col IN keyfields %]
  lookup                = [%- col -%]
 [%- END -%]
[%- END %]
[%- IF columns.size == 1 -%]
 [% FOR col IN columns %]
  column                = [ [%- col -%] ]
 [%- END -%]
[%- ELSE -%]
 [% FOR col IN columns %]
  column                = [%- col -%]
 [%- END -%]
[%- END %]
</list_header>

# Define bindings for Entry widgets mapped to fields. Multiple fields
# are allowed.  Force array of single field elements by surrounding
# the value of the config entry with [].
<bindings/>

# Define bindings for TM cells. Multiple fields can be added
# Force array of single elements for 'field' by surrounding the value
# of the config entry by []
<tablebindings/>

# Table attributes
# Main table
<maintable>
    name                = [% maintable.name %]
    view                = [% maintable.view %]
    <keys>
    [%- IF keyfields.size == 1 -%]
     [% FOR col IN keyfields %]
      name              = [ [%- col -%] ]
     [%- END -%]
    [%- ELSE -%]
     [% FOR col IN keyfields %]
      name              = [%- col -%]
     [%- END -%]
    [%- END %]
    </keys>
    <columns>
    [%- FOR column IN maintable.columns %]
        <[% column.name %]>
            label       = [% column.label %]
            state       = [% column.state %]
            ctrltype    = [% column.ctrltype %]
            displ_width = [% column.displ_width %]
            valid_width = [% column.valid_width %]
            numscale    = [% column.numscale %]
            readwrite   = [% column.readwrite %]
            findtype    = [% column.findtype %]
            bgcolor     = [% column.bgcolor %]
            datatype    = [% column.datatype %]
        </[% column.name %]>
    [%- END %]
    </columns>
</maintable>

# Dependent tables with TableMatrix label
[%- SET count = 1 -%]
[% FOR deptable IN deptables %]
<deptable tm[% count %] >
    name                = [% deptable.name %]
    view                = [% deptable.view %]
    updatestyle         = delete+add
    selectorcol         =
    colstretch          =
    <orderby>
    [%- IF deptable.orderby.size == 1 -%]
     [% FOR col IN deptable.orderby %]
      name              = [ [%- col -%] ]
     [%- END -%]
    [%- ELSE -%]
     [% FOR col IN deptable.orderby %]
      name              = [%- col -%]
     [%- END -%]
    [%- END %]
    </orderby>
    <keys>
    [%- IF deptable.keyfields.size == 1 -%]
     [% FOR col IN deptable.keyfields %]
      name              = [ [%- col -%] ]
     [%- END -%]
    [%- ELSE -%]
     [% FOR col IN deptable.keyfields %]
      name              = [%- col -%]
     [%- END -%]
    [%- END %]
    </keys>
    <columns>
    [%- FOR column IN deptable.columns %]
        <[% column.name %]>
            id          = [% column.id %]
            label       = [% column.label %]
            tag         = [% column.tag %]
            displ_width = [% column.displ_width %]
            valid_width = [% column.valid_width %]
            numscale    = [% column.numscale %]
            readwrite   = [% column.readwrite %]
            datatype    = [% column.datatype %]
        </[% column.name %]>
    [%- END %]
    </columns>
</deptable>
[%- SET count = count + 1 -%]
[%- END %]
[%- IF count <= 1 %]
<deptable/>
[%- END %]

[% IF deptables.size > 0 -%]
# The toolbar atached to the tm1 TableMatrix. Preserves order.
<scrtoolbar>
    <tm1>
        name            = tb2ad
        method          = tmatrix_add_row
    </tm1>
    <tm1>
        name            = tb2rm
        method          = tmatrix_remove_row
    </tm1>
</scrtoolbar>
[% ELSE -%]
# The toolbar atached to the tm1 TableMatrix. Preserves order.
<scrtoolbar/>
[%- END %]

# Change main toolbar behavior
<toolbar/>
